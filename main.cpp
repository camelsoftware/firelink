#include "firelink.h"
#include "test_vehicle.h"
#include "test_station.h"

#include "test.h"

#include <iostream>
#include <bitset>

using namespace std;
using namespace FireLink;

FireLink::VehicleData vvd;
myvehicle vehicle(&vvd);
mystation station;

void print_datagram(unsigned char* dg)
{
    unsigned char pack[20];
    memcpy(pack, dg, 20);

    int i;
    for(i = 0; i <= 19; i++)
        cout << hex << int(pack[i]) << " ";

    cout << dec << endl;
}

void print_datagram(unsigned char* dg, int sz)
{
    unsigned char pack[sz];
    memcpy(pack, dg, sz);

    int i;
    for(i = 0; i <= sz; i++)
        cout << hex << int(pack[i]) << " ";

    cout << dec << endl;
}

bool compare_floats(float a, float b, float ep)
{
    if(fabs(a-b) < ep)
        return true;
    return false;
}

bool main_loop()
{
    for(int i = 0; i < 6; i++)
    {
        unsigned char packbuf[20];
        vehicle.next_pack(packbuf);

        if(station.read_pack(packbuf) == -1)
            return false;

        station.next_pack(packbuf);
        if(vehicle.read_pack(packbuf) == -1)
             return false;
    }
    return true;
}

bool encode_test(std::string& msg)
{
    msg = "encode()";

    unsigned char packbuf[40];
    for(int k = 0; k < 1000; k++)
    {
        for(int j = 0; j < 20; j++)
        {
            station.next_pack(&packbuf[j]);

            bool gotpack = false;
            for(int i = 0; i < 40; i++)
            {
                if(vehicle.encode(packbuf[i]))
                    gotpack = true;
            }
            if(!gotpack)
                return false;
        }
    }

    return true;
}
bool stream_test(std::string& msg)
{
    msg = "volts main loop failed";
    vehicle.get_vehicle_data()->volts = 126;
    if(!main_loop())
        return false;

    msg = "volts is incorrect";
    if(!compare_floats(station.get_volts(), 12.6, 0.01))
        return false;

    msg = "milliamps";
    vehicle.get_vehicle_data()->milliamps = 12653;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_amps(), 12.653, 0.0001))
        return false;

    msg = "timer";
    vehicle.get_vehicle_data()->timer = 64253;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_timer(), 6425.3, 0.01))
        return false;

    msg = "temperature";
    vehicle.get_vehicle_data()->temperature = 6453;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_temperature(), 64.53, 0.001))
        return false;

    msg = "power remaining";
    vehicle.get_vehicle_data()->power_remaining = 64;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_power_remaining(), 64, 0.01))
        return false;

    msg = "motor armed";
    vehicle.get_vehicle_data()->motor_armed = false;
    if(!main_loop())
        return false;
    if(station.get_motor_armed())
        return false;

    vehicle.get_vehicle_data()->motor_armed = true;
    if(!main_loop())
        return false;
    if(!station.get_motor_armed())
        return false;

    msg = "autothrottle on";
    vehicle.get_vehicle_data()->autothrottle_on = true;
    if(!main_loop())
        return false;

    if(!station.get_autothrottle_on())
        return false;

    msg = "hil mode on";
    vehicle.get_vehicle_data()->hil_mode = true;
    if(!main_loop())
        return false;

    if(!station.get_hil_mode_on())
        return false;

    msg = "ground track";
    vehicle.get_vehicle_data()->ground_track = 26583;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_ground_track(), 265.83, 0.001))
        return false;

    msg = "heading, pitch, roll";
    vehicle.get_vehicle_data()->heading = 9083;
    vehicle.get_vehicle_data()->pitch = 2354;
    vehicle.get_vehicle_data()->roll = 3563;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_heading(), 90.83, 0.001))
        return false;
    if(!compare_floats(station.get_pitch(), 23.54, 0.001))
        return false;
    if(!compare_floats(station.get_roll(), 35.63, 0.001))
        return false;

    msg = "altitude";
    vehicle.get_vehicle_data()->alt = -734;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_alt(), -734.0, 0.1))
        return false;

    vehicle.get_vehicle_data()->alt = 63134;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_alt(), 63134, 0.1))
        return false;

    msg = "vert speed";
    vehicle.get_vehicle_data()->vert_speed = 5215;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_vert_speed(), 52.15, 0.001))
        return false;

    msg = "air speed";
    vehicle.get_vehicle_data()->air_speed = 5215;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_air_speed(), 52.15, 0.001))
        return false;

    msg = "lat, lng";
    vehicle.get_vehicle_data()->latitude = 52156542;
    vehicle.get_vehicle_data()->longitude = 152678912;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_latitude(), 52.156542, 0.0000001))
        return false;
    if(!compare_floats(station.get_longitude(), 152.678912, 0.0000001))
        return false;

    msg = "ground speed";
    vehicle.get_vehicle_data()->ground_speed = 5254;
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_ground_speed(), 52.54, 0.001))
        return false;

    msg = "satellites used";
    vehicle.get_vehicle_data()->satellites_used = 14;
    if(!main_loop())
        return false;
    if(station.get_satellites_used() != 14)
        return false;

    msg = "ap mode";
    vehicle.get_vehicle_data()->ap_mode = FireLink::AP_MODE_WAYPOINTS;
    if(!main_loop())
        return false;
    if(station.get_ap_mode() != FireLink::AP_MODE_WAYPOINTS)
        return false;

    msg = "ground track";

    for(float i = -180; i < 180; i += 1.0)
    {
        vehicle.get_vehicle_data()->ground_track = i*100;
        if(!main_loop())
            return false;
        if(!compare_floats(station.get_ground_track(), i, 0.001))
        {
            cout << endl << station.get_ground_track() << " " << i << endl;
            return false;
        }
    }

    msg = "selected pitch";
    station.push_set_selected_pitch_and_roll(50.00, -45.00);
    if(!main_loop())
        return false;

    if(vehicle.sel_pitch != 5000)
        return false;
    msg = "selected roll";
    if(vehicle.sel_roll != -4500)
        return false;

    msg = "selected heading";
    station.push_set_selected_heading_and_alt(-180.0, 5000);
    if(!main_loop())
        return false;

    if(vehicle.sel_heading != -18000)
        return false;
    msg = "selected alt";
    if(vehicle.sel_alt != 5000)
        return false;

    return true;
}

bool stream_packet_mask_test(std::string& msg)
{
    msg = "Turning off ap_status_2";
    vehicle.stream_pack_mask(FireLink::VS_AP_STATUS2, false);
    vehicle.get_vehicle_data()->wp_lat = 54777888;
    if(!main_loop())
        return false;
    if(compare_floats(station.get_ap_waypoint_lat(), 54.777888, 0.0000001))
       return false;

    msg = "Turning on ap_status_2";
    vehicle.stream_pack_mask(FireLink::VS_AP_STATUS2, true);
    if(!main_loop())
        return false;
    if(!compare_floats(station.get_ap_waypoint_lat(), 54.777888, 0.0000001))
       return false;

    msg = "Station setting packet mask";
    station.set_stream_pack_mask(VS_INST_1, true);
    station.set_stream_pack_mask(VS_INST_2, true);
    station.set_stream_pack_mask(VS_POWER, true);
    station.set_stream_pack_mask(VS_AP_STATUS, true);
    station.set_stream_pack_mask(VS_AP_STATUS2, true);
    station.push_stream_pack_mask();

    if(!main_loop())
        return false;

    msg = "Station setting packet mask";
    station.set_stream_pack_mask(VS_INST_1, false);
    station.set_stream_pack_mask(VS_INST_2, false);
    station.set_stream_pack_mask(VS_POWER, false);
    station.set_stream_pack_mask(VS_AP_STATUS, false);
    station.set_stream_pack_mask(VS_AP_STATUS2, false);
    station.set_stream_pack_mask(VS_RAW_ACC_MAG, true);
    station.push_stream_pack_mask();

    if(!main_loop())
        return false;

    msg = "Station setting packet mask";
    station.set_stream_pack_mask(VS_INST_1, true);
    station.set_stream_pack_mask(VS_INST_2, true);
    station.set_stream_pack_mask(VS_POWER, true);
    station.set_stream_pack_mask(VS_AP_STATUS, true);
    station.set_stream_pack_mask(VS_AP_STATUS2, true);
    station.set_stream_pack_mask(VS_RAW_ACC_MAG, false);
    station.push_stream_pack_mask();

    if(!main_loop())
        return false;

    return true;
}

bool packet_tests(std::string& msg)
{
    msg = "Keep alive";
    station.push_keep_alive_pack(5);
    if(!main_loop())
        return false;
    if(vehicle.keep_alive_seconds != 5)
        return false;

    msg = "Arm motor";
    station.push_arm_motor_pack(true);
    if(!main_loop())
        return false;
    if(!station.get_motor_armed())
        return false;

    station.push_arm_motor_pack(false);
    if(!main_loop())
        return false;
    if(station.get_motor_armed())
        return false;


    msg = "Add waypoint";
    station.push_add_waypoint(10, 45.24353, 65.36643, 400);
    if(!main_loop())
        return false;
    if(vehicle.last_wp_n != 10)
        return false;
    if(!compare_floats(vehicle.last_wp_lat, 45.24353, 0.0000001))
        return false;
    if(!compare_floats(vehicle.last_wp_lng, 65.36643, 0.0000001))
        return false;
    if(vehicle.last_wp_alt != 400)
        return false;

    msg = "Add waypoint2";
    station.push_add_waypoint(11, 45.24353, 65.36643, 400);
    if(!main_loop())
        return false;
    if(vehicle.last_wp_n != 11)
        return false;
    if(!compare_floats(vehicle.last_wp_lat, 45.24353, 0.0000001))
        return false;
    if(!compare_floats(vehicle.last_wp_lng, 65.36643, 0.0000001))
        return false;
    if(vehicle.last_wp_alt != 400)
        return false;

    msg = "Add geofence point";
    station.push_add_geofence_point(1, 25.444444, 26.555555);
    if(!main_loop())
        return false;
    if(vehicle.last_gp_n != 1)
        return false;
    if(!compare_floats(vehicle.last_gp_lat, 25.444444, 0.0000001))
        return false;
    if(!compare_floats(vehicle.last_gp_lng, 26.555555, 0.0000001))
        return false;

    msg = "GET geofence point";
    station.push_get_geofence_point(1);
    if(!main_loop())
    {
        cout << endl << "main loop failed" << endl;
        return false;
    }
    if(station.last_gp_n != 1)
        return false;
    if(!compare_floats(station.last_gp_lat, 25.252525, 0.0000001))
        return false;
    if(!compare_floats(station.last_gp_lng, 35.353535, 0.0000001))
        return false;

    msg = "GET number of geofence points";
    station.push_get_n_geofence_points();
    if(!main_loop())
        return false;
    if(station.n_gp_points != 12)
        return false;

    msg = "Remove geopoints";
    station.push_remove_geopoint(15);
    vehicle.last_remove_geopoint = 0;
    if(!main_loop())
        return false;
    if(vehicle.last_remove_geopoint != 15)
        return false;

    msg = "Clear geopoints";
    station.push_clear_geopoints();
    vehicle.geopoints_cleared = false;
    if(!main_loop())
        return false;
    if(!vehicle.geopoints_cleared)
        return false;

    msg = "Setting alarm actions";
    station.push_set_alarm_actions(ALARM_ACTION_RTB, ALARM_ACTION_NO_PWR_GLIDE,
                                   ALARM_ACTION_FIXED_BANK_LOITER,
                                   ALARM_ACTION_RTB, ALARM_ACTION_NO_PWR_GLIDE);
    if(!main_loop())
        return false;
    msg = "setting low bat alarm action";
    if(vehicle.low_bat_action != ALARM_ACTION_RTB)
        return false;
    msg = "setting boundry alarm action";
    if(vehicle.boundary_action != ALARM_ACTION_NO_PWR_GLIDE)
        return false;
    msg = "setting no gps alarm action";
    if(vehicle.no_gps_action != ALARM_ACTION_FIXED_BANK_LOITER)
        return false;
    msg = "setting lost control alarm action";
    if(vehicle.lost_control_action != ALARM_ACTION_RTB)
        return false;
    msg = "setting no telem alarm action";
    if(vehicle.no_telem_action != ALARM_ACTION_NO_PWR_GLIDE)
        return false;

    msg = "Getting alarm actions";
    station.push_get_alarm_actions();
    if(!main_loop())
        return false;
    if(station.low_bat_action != ALARM_ACTION_RTB)
        return false;
    if(station.boundary_action != ALARM_ACTION_NO_PWR_GLIDE)
        return false;
    if(station.no_gps_action != ALARM_ACTION_FIXED_BANK_LOITER)
        return false;
    if(station.lost_control_action != ALARM_ACTION_RTB)
        return false;

    msg = "Vehicle alarm sending";
    vehicle.push_alarm_activated(ALARM_TYPE_BOUNDARY);
    if(!main_loop())
        return false;
    if(station.last_alarm != ALARM_TYPE_BOUNDARY)
        return false;
    vehicle.push_alarm_activated(ALARM_TYPE_NO_SETTINGS);
    if(!main_loop())
        return false;
    if(station.last_alarm != ALARM_TYPE_NO_SETTINGS)
        return false;

    if(!main_loop())
        return false;

    msg = "Setting max euler rates";
    station.push_set_max_euler_rates(5.0, 5.7);
    station.push_get_max_euler_rates();

    if(!main_loop())
        return false;
    if(!(compare_floats(station.max_pitch, 5.0, 0.001)))
        return false;
    if(!(compare_floats(station.max_roll, 5.7, 0.001)))
        return false;

    msg = "Speed gains";
    station.push_set_speed_gain(0.0052);
    station.push_get_speed_gain();

    if(!main_loop())
        return false;
    if(!compare_floats(station.speed_gain, 0.0052, 0.00001))
        return false;

    station.push_get_servo_configuration();
    if(!main_loop())
        return false;

    msg = "Setting turn coord gains";
    station.push_set_turn_coord_ratios(0.5, 0.01);
    if(!main_loop())
        return false;

    msg = "vert speed pids";
    station.push_set_vert_speed_gains(0.5, 0.64, 16.326);
    station.push_get_vert_speed_gains();
    if(!main_loop())
        return false;
    msg = "comparing vert speed floats";
    if(!(compare_floats(0.5, station.vs_kp, 0.0001)))
        return false;
    if(!(compare_floats(0.64, station.vs_ki, 0.0001)))
        return false;
    if(!(compare_floats(16.326, station.vs_kd, 0.0001)))
        return false;

    msg = "mag cal";
    fm_mag_cal_pack mcp;
    mcp.x_bias = 500;
    mcp.y_bias = 600;
    mcp.z_bias = 700;
    mcp.x_gain = 400;
    mcp.y_gain = 300;
    mcp.z_gain = 200;

    station.push_set_mag_cal(&mcp);
    if(!main_loop())
        return false;

    if(vehicle.mcp.x_bias != 500)
        return false;

    station.push_get_mag_cal();
    if(!main_loop())
        return false;
    if(!main_loop())
        return false;


    station.push_set_accel_cal(0.23, 0.10, 1.06);
    if(!main_loop())
        return false;



    return true;
}

bool repeat_pack_test(std::string& msg)
{
    msg = "Repeat packet test";
    fl_pack pack;
    uint16_t pid = 5342;
    pack.id = pid;

    vehicle.clear_queue();
    station.clear_queue();
    station.clear_in_queue();

    vehicle.push_custom_pack(pack);

    for(int i = 0; i < 10; i++)
    {
        unsigned char buf[20];
        vehicle.next_pack(buf);
        station.read_pack(buf);
    }
    if(station.repeat_pack != 1)
        return false;

    return true;
}

bool debug_msg_test(std::string& msg)
{
    msg = "Message test";
    vehicle.push_debug_message("Hello world this is a very long message. Hello world this is a very long message. Hello world this is a very long message.");

    if(!main_loop())
        return false;
    if(!main_loop())
        return false;
    if(!main_loop())
        return false;
    if(!main_loop())
        return false;
    if(!main_loop())
        return false;
    if(!main_loop())
        return false;
    if(!main_loop())
        return false;
    return true;
}


int main()
{
    unsigned char key[32] = "2222222222222222222222222222211";
    unsigned char iv[16] = "111111111111111";
    vehicle.set_key(key);
    station.set_key(key);
    vehicle.set_iv(iv);
    station.set_iv(iv);

/**
    vehicle.encryption_on = true;
    station.encryption_on = true;
    */

    Camlib::TestHarness tst("FireLink Test Harness");


    tst.add_module(encode_test, "Encode() test");
	tst.add_module(repeat_pack_test, "Repeat pack test");
	tst.add_module(stream_test, "Streamed Data Test");
	tst.add_module(stream_packet_mask_test, "Streamed Packet Mask Test");
	tst.add_module(packet_tests, "Packet Tests");
	tst.add_module(debug_msg_test, "Debug Message Test");

	tst.run();
}
