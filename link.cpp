/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include "link.h"
#include "version.h"

namespace FireLink
{

//utility function to round floats/doubles to nearest int
int roundf(double r)
{
    return (r > 0.0) ? (r + 0.5) : (r - 0.5);
}

const char* version()
{
    return AutoVersion::FULLVERSION_STRING;
}

VehicleData::VehicleData()
{
    timer = 0;
    volts = 0;
    milliamps = 0;
    temperature = power_remaining = 0;
    motor_armed = true;

    heading = pitch = roll = alt = 0;
    vert_speed = air_speed = 0;

    latitude = longitude = 0;
    ground_speed = 0;
    satellites_used = 0;
    gps_mode = GPS_MODE_NONE;

    ap_mode = AP_MODE_MANUAL;
    ap_pitch = ap_roll = ap_heading = 0;
    ap_alt = 0;
}


Link::Link()
{
    encryption_on = false;
    queue.pNext = 0;
    in_buf_pos = 0;
    vd = nullptr;
}

Link::~Link()
{
    clear_queue();
}

uint16_t Link::push_custom_pack(fl_pack pack)
{
    PackQueue* ptr = &queue;
    int ctr = 0;
    while((ptr->pNext != 0) && (ctr < 100))
    {
        ptr = ptr->pNext;
        ctr++;
    }
    //don't allocate more than 100 packets worth of memory
    if(ctr >= 99)
        return 0;

    //uses malloc/free instead of new/delete because they don't throw exceptions
    //this reduces overhead on embedded systems
    ptr->pNext = (PackQueue*)malloc(sizeof(PackQueue));
    ptr->pNext->pb = pack;
    ptr->pNext->pNext = 0;

    if(not on_queue(128))
    {
        fl_pack fpack;
        fpack.id = 128;
        push_custom_pack(fpack);
    }

    return pack.id;
}

bool Link::on_queue(uint16_t id)
{
    PackQueue* ptr = &queue;
    while(ptr->pNext != 0)
    {
        if(ptr->pNext->pb.id == id)
            return true;
        ptr = ptr->pNext;
    }
    return false;
}

fl_pack Link::next_off_queue()
{
    if(queue.pNext != 0)
        return queue.pNext->pb;

    fl_pack pp;
    return pp;
}

bool Link::remove_pack(uint16_t id)
{
    PackQueue* ptr = &queue;
    while(ptr->pNext)
    {
        if(ptr->pNext->pb.id == id)
        {
            PackQueue* tmpptr = ptr->pNext;
            ptr->pNext = ptr->pNext->pNext;
            free(tmpptr);
            return true;
        }
        ptr = ptr->pNext;
    }
    return false;
}

void Link::clear_queue()
{
    PackQueue* ptr = &queue;
    while(ptr->pNext != 0)
    {
        PackQueue* tmpptr = ptr->pNext;
        ptr->pNext = ptr->pNext->pNext;
        free(tmpptr);
    }
}

int Link::queue_size()
{
    PackQueue* ptr = &queue;
    uint8_t ret = 0;
    while(ptr->pNext)
    {
        ptr = ptr->pNext;
        ret++;
    }
    return ret;
}

void Link::decrypt_pack(unsigned char* data_in, unsigned char* data_out)
{
    if(encryption_on)
    {
        unsigned char IV[16];
        memcpy(IV, init_vec, 16);
        aes.cbc_decrypt(data_in, data_out, 1, IV);
    }
    else
    {
        memcpy(data_out, data_in, 16);
    }
}

int32_t Link::read_pack(unsigned char* pack)
{
    unsigned char decrypt_buf[16];
    unsigned char* data;
    uint16_t id;

    if(!check_checksum(pack))
        return false;

#ifndef NO_ENCRYPTION
    if(encryption_on)
    {
        //decrypt
        unsigned char IV[16];
        memcpy(IV, init_vec, 16);
        aes.cbc_decrypt(pack, decrypt_buf, 1, IV);
    }
    else
    {
        memcpy(decrypt_buf, pack, 16);
    }
#else
    memcpy(decrypt_buf, pack, 16);
#endif

    memcpy(&id, decrypt_buf, 2);
    data = decrypt_buf + 2;

    if(id == 128)
        return 128;
    if(id > 1024)
    {
        if(on_custom_recv(id, data))
            return id;
    }

    return -1;
}

int16_t Link::make_checksum(unsigned char* data_p)
{
    int length = 16;
    unsigned char i;
    unsigned int data;
    unsigned int crc;

    crc = 0xffff;

    if (length == 0)
        return (~crc);

    do
    {
        for (i = 0, data = (unsigned int)0xff & *data_p++;
                i < 8;
                i++, data >>= 1)
        {
            if ((crc & 0x0001) ^ (data & 0x0001))
                crc = (crc >> 1) ^ 0x8408;
            else
                crc >>= 1;
        }
    }
    while (--length);

    crc = ~crc;
    data = crc;
    crc = (crc << 8) | (data >> 8 & 0xFF);

    return (crc);
}

bool Link::check_checksum(unsigned char* data_p)
{
    uint16_t cs = make_checksum(data_p);
    uint16_t cs2;
    memcpy(&cs2, data_p+16, 2);
    return (cs == cs2);
}


void Link::set_iv(unsigned char* iv)
{
#ifndef NO_ENCRYPTION
    memcpy(init_vec, iv, 16);
#endif
}

void Link::set_key(unsigned char* key)
{
#ifndef NO_ENCRYPTION
    aes.set_key(key, 256);
#endif
}

uint8_t Link::read_bit(uint8_t bte, uint8_t n)
{
    bool on = bte & (1 << (n-1));

    return on;
}

uint8_t Link::set_bit(uint8_t bte, uint8_t n, bool on)
{
    uint8_t ret = bte;
    if(on)
        ret |= 1 << (n-1);
    else
        ret &= ~(1 << (n-1));

    return ret;
}

void Link::set_disable_stream(bool stream)
{
    no_stream = stream;
}
bool Link::get_disable_stream()
{
    return no_stream;
}

int32_t Link::encode(unsigned char b)
{
    in_buf[in_buf_pos] = b;
    in_buf_pos++;

    if(in_buf_pos > 18)
    {
        if((in_buf[in_buf_pos] == 0x00) && (in_buf[in_buf_pos-1] == 0x0A))
        {
            int32_t ret = read_pack(&in_buf[in_buf_pos-19]);
            if(ret)
            {
                memset(in_buf, '\0', 60);
                in_buf_pos = 0;
            }
            return ret;
        }
    }
    if(in_buf_pos >= 60)
    {
        memset(in_buf, '\0', 60);
        in_buf_pos = 0;
    }
    return -1;
}

};

