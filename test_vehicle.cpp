#include "test_vehicle.h"
#include <iostream>
using namespace std;

myvehicle::myvehicle(VehicleData* vd) : Vehicle(vd)
{
    vd->acc_mag.acc_z = 564;
}

bool myvehicle::on_keep_alive_recv(uint32_t s)
{
    keep_alive_seconds = s;
    return true;
}

bool myvehicle::on_arm_motor_recv(bool arm)
{
    vd->motor_armed = arm;
    return true;
}

bool myvehicle::on_add_waypoint(uint16_t n, float lat, float lng, int32_t alt)
{
    last_wp_n = n;
    last_wp_lat = lat;
    last_wp_lng = lng;
    last_wp_alt = alt;
    return true;
}

bool myvehicle::on_add_geofence_point(uint16_t n, float lat, float lng)
{
    last_gp_n = n;
    last_gp_lat = lat;
    last_gp_lng = lng;
    return true;
}

bool myvehicle::on_get_geofence_point(uint16_t n)
{
    push_geofence_point_report(n, 25.252525, 35.353535);
    return true;
}

bool myvehicle::on_get_n_geofence_points()
{
    push_n_geofence_points(12);
    return true;
}

bool myvehicle::on_remove_geopoint(uint16_t n)
{
    last_remove_geopoint = n;
    return true;
}

bool myvehicle::on_clear_geopoints()
{
    geopoints_cleared = true;
    return true;
}

bool myvehicle::on_set_alarm_actions(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem)
{
    low_bat_action = lb;
    boundary_action = ba;
    no_gps_action = gpsa;
    lost_control_action = lca;
    no_telem_action = no_telem;
    return true;
}

bool myvehicle::on_get_alarm_actions()
{
    push_alarm_actions_report(low_bat_action, boundary_action, no_gps_action, lost_control_action, no_telem_action);
    return true;
}

bool myvehicle::on_set_max_euler_rates(uint16_t pitch, uint16_t roll)
{
    max_pitch_rate = pitch;
    max_roll_rate = roll;
    return true;
}

bool myvehicle::on_get_max_euler_rates()
{
    push_max_euler_rates_report(max_pitch_rate, max_roll_rate);
    return true;
}

bool myvehicle::on_set_speed_gain(float sg)
{
    speed_gain = sg;
    return true;
}

bool myvehicle::on_get_speed_gain()
{
    push_speed_gain_report(speed_gain);
    return true;
}

bool myvehicle::on_get_servo_configuration()
{
    push_servo_configuration_report(5, 8, 9, 10, 11, 12, 13, 14, 15);
    return true;
}

bool myvehicle::on_set_turn_coord_ratios(float r, float p)
{
    return true;
}

bool myvehicle::on_set_vert_speed_gains(float kp, float ki, float kd)
{
    vs_kp = kp;
    vs_ki = ki;
    vs_kd = kd;
    return true;
}

bool myvehicle::on_get_vert_speed_gains()
{
    push_vert_speed_gains_report(vs_kp, vs_ki, vs_kd);
    return true;
}

bool myvehicle::on_set_accel_cal(float x, float y, float z)
{
    acc_x = x;
    acc_y = y;
    acc_z = z;
    return true;
}

bool myvehicle::on_get_accel_cal()
{
    push_accel_cal(acc_x, acc_y, acc_z);
    return true;
}

bool myvehicle::on_get_mag_cal()
{
    push_mag_cal(&mcp);
    return true;
}

bool myvehicle::on_set_mag_cal(fm_mag_cal_pack* f)
{
    memcpy(&mcp, f, sizeof(fm_mag_cal_pack));
    return true;
}

bool myvehicle::on_set_selected_heading_and_alt(int16_t hdg, int32_t alt)
{
    sel_heading = hdg;
    sel_alt = alt;
    return true;
}

bool myvehicle::on_set_selected_pitch_and_roll(int16_t pitch, int16_t roll)
{
    sel_pitch = pitch;
    sel_roll = roll;
    return true;
}


