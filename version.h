#ifndef VERSION_H
#define VERSION_H

namespace AutoVersion{
	
	//Date Version Types
	static const char DATE[] = "20";
	static const char MONTH[] = "09";
	static const char YEAR[] = "2014";
	static const char UBUNTU_VERSION_STYLE[] = "14.09";
	
	//Software Status
	static const char STATUS[] = "Release";
	static const char STATUS_SHORT[] = "r";
	
	//Standard Version Type
	static const long MAJOR = 1;
	static const long MINOR = 3;
	static const long BUILD = 380;
	static const long REVISION = 2167;
	
	//Miscellaneous Version Types
	static const long BUILDS_COUNT = 631;
	#define RC_FILEVERSION 1,3,380,2167
	#define RC_FILEVERSION_STRING "1, 3, 380, 2167\0"
	static const char FULLVERSION_STRING[] = "1.3.380.2167";
	
	//These values are to keep track of your versioning state, don't modify them.
	static const long BUILD_HISTORY = 80;
	

}
#endif //VERSION_H
