#include "test_station.h"

#include <iostream>
using namespace std;


bool mystation::on_geofence_point_reported(uint16_t n, float lat, float lng)
{
    last_gp_n = n;
    last_gp_lat = lat;
    last_gp_lng = lng;
    return true;
}

bool mystation::on_n_geofence_points_reported(uint16_t n)
{
    n_gp_points = n;
    return true;
}

bool mystation::on_alarm_actions_reported(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem)
{
    low_bat_action = lb;
    boundary_action = ba;
    no_gps_action = gpsa;
    lost_control_action = lca;
    no_telem_action = no_telem;
    return true;
}

bool mystation::on_alarm_activated(ALARM_TYPE alarm)
{
    last_alarm = alarm;
    return true;
}


bool mystation::on_custom_recv(uint16_t pid, unsigned char* data)
{
    if(pid == 5342)
        repeat_pack++;
    return true;
}

bool mystation::on_max_euler_rates_reported(float pitch, float roll)
{
    max_pitch = pitch;
    max_roll = roll;
    return true;
}

bool mystation::on_speed_gain_reported(float sg)
{
    speed_gain = sg;
    return true;
}

bool mystation::on_servo_configuration_reported(float s, int8_t a, int8_t a1, int8_t a2, int8_t a3, int8_t a4, int8_t a5, int8_t a6, int8_t a7)
{
    return true;
}

bool mystation::on_vert_speed_gains_reported(float kp, float ki, float kd)
{
    vs_kp = kp;
    vs_ki = ki;
    vs_kd = kd;
    return true;
}


bool mystation::on_debug_msg_received(char* msg)
{
    return true;
}
