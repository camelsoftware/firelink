/**
 * \file link.h
 * \brief This file contains all of the essentials for both Station and Vehicle.
 *
**/

/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef LINK_H_INCLUDED
#define LINK_H_INCLUDED

//#define NO_ENCRYPTION
#define QUEUE_SIZE 15

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef NO_ENCRYPTION
#include "aes.h"
#endif

#include "firelink_types.h"


namespace FireLink
{

//utility function
int roundf(double r);

const char* version();

/**
 * \brief struct to contain packet data
 */

struct fl_pack
{
    uint16_t id;        /*!< The packet ID. */

    uint8_t data[12];   /*!< Packet data. These 12 bytes need to be packed / unpacked using the appropriate struct. The struct to use depends on the packet ID.
                        * Not all packets actually contain data, sometimes just the ID is enough information.
                        * For those packets there is no corresponding struct and the data can be ignored.<br>
                        * <b>Station Packet IDs - Struct Reference</b> <br>
                        * SC_KEEP_ALIVE - sm_keep_alive <br>
                        * SC_ARM_MOTOR - sm_arm_motor <br>
                        * SC_SET_AP_MODE - sm_set_ap_mode <br>
                        * SC_GET_SATELLITE_REPORT - sm_GET_satellite_report <br>
                        * SC_SET_ALTIMETER_SOURCE - sm_set_altimeter_source <br>
                        * SC_SET_HOME_COORDINATES - sm_set_home_coordinates <br>
                        * SC_REMOVE_WAYPOINT - sm_remove_waypoint <br>
                        * SC_GET_WAYPOINT - sm_get_waypoint <br>
                        * SC_SET_AHRS_INSTALLATION_OFFSET - sm_set_ahrs_installation_offset <br>
                        * SC_SET_SERVO_CONFIG - sm_set_servo_config <br>
                        * SC_SET_MIXER_CONFIG - sm_set_mixer_config <br>
                        * SC_SET_AP_GAIN_CONFIG - sm_set_ap_gain_config <br>
                        * SC_SET_TRIM_CONFIG - sm_set_trim_config <br>
                        * SC_SET_MAG_VAR - sm_set_mag_var <br>
                        * SC_SET_HEADING_AND_ALT_GAIN - heading_and_alt_gain_pack <br>
                        * SC_SET_LOITER_SETTINGS - loiter_settings_pack <br>
                        * SC_ADD_WAYPOINT to SC_ADD_WAYPOINT+512 - sm_add_waypoint <br><br>
                        * <b>Vehicle Packet IDs - Struct Reference </b> <br>
                        * VC_SATELLITE_REPORT - vm_satellite_report<br>
                        * VC_HOME_COORDINATES - vm_home_coordinates<br>
                        * VC_AUTOPILOT_STATUS_CHANGE - vm_autopilot_status_change<br>
                        * VC_WAYPOINT_REPORT - vm_waypoint_report<br>
                        * VC_N_WAYPOINTS - vm_n_waypoints<br>
                        * VC_AHRS_OFFSET_REPOT - vm_ahrs_offset_report <br>
                        * VC_AHRS_QUATERNION_REPORT - vm_ahrs_quaternion_report <br>
                        * VC_SERVO_CONFIG_REPORT - vm_servo_config_report <br>
                        * VC_MAG_CAL_REPORT - vm_mag_cal_report <br>
                        * VC_MIXER_CONFIG_REPORT - vm_mixer_config_report <br>
                        * VC_AP_GAIN_CONFIG_REPORT - vm_ap_gain_config_report <br>
                        * VC_TRIM_CONFIG_REPORT - vm_trim_config_report <br>
                        * VC_MAG_VAR_REPORT - vm_mag_var_report <br>
                        * VC_HEADING_AND_ALT_GAIN_REPORT - heading_and_alt_gain_pack <br>
                        * VC_LOITER_SETTINGS_REPORT - loiter_settings_pack <br>
                        */
};


/**
 * \brief Packet queue struct - the packet queue is simply a linked list.
 */

struct PackQueue
{
    fl_pack pb;         /*!< The packet in queue. */
    PackQueue* pNext;   /*!< A pointer to next item in list, or 0. */
};


/**
 * \brief The VehicleData class contains the data that is streamed from the vehicle to the station.
 */
class VehicleData
{
    public:
        VehicleData();

        uint16_t volts;                  	/*!< Battery voltage in tenths. */
		uint16_t timer;						/*!< Timer since vehicle started to operate in tenths of a second.*/
        uint16_t milliamps;                 /*!< Milliamps drawn off main battery or power supply in hundredths of an amp. */
        int16_t temperature;                /*!< On board temperature in hundredths of a degree. */
        uint8_t power_remaining;            /*!< A percentage from 0 - 100 that represents how much power or fuel is remaining. */
        bool motor_armed;                   /*!< True is the motor is armed, false is motor is disarmed (it's a safety thing). */
        bool autothrottle_on;               /*!< True is when the autothrottle is on, false is off. */
        bool hil_mode;                      /*!< True when HIL mode is on. */
        bool has_rc;                        /*!< True when RC signal is present. */
        bool inside_boundary;               /*!< True when vehicle is inside the geofence. */
        bool autolaunch_on;                 /*!< True when autolaunch is on. */
        int16_t ground_track;              /*!< Track over ground in hundredths. -18000 to +18000. */

        int16_t heading, pitch, roll;       /*!< Heading, pitch and roll expressed in hundredths of a degree. Referenced to magnetic north. */
        int32_t alt;                            /*!< Pressure altitude in feet. This variable is turned into a uint16_t before being transmitted.
                                                    A valid range, therefore, is -1000 ft to 64,534 ft which should be enough for most people. */
        int16_t vert_speed;                 /*!< Vertical speed in hundredths of feet per second. */
        uint16_t air_speed;                 /*!< Air speed in hundredths of knots. */

        int32_t latitude, longitude;        /*!< Latitude and longitude in millions of a degree. */
        int16_t ground_speed;               /*!< Ground speed in hundredths of km/hour. */
        uint8_t satellites_used;            /*!< Number of GPS satellites in use. */
        GPS_MODE gps_mode;                  /*!< GPS mode of operation. */

        AUTOPILOT_MODE ap_mode;             /*!< Autopilot mode. */
        int16_t ap_pitch, ap_roll, ap_heading; /*!< Autopilot bugs. These bugs point to the pitch, roll and heading selected by the autopilot. */
        int32_t ap_alt;                     /*!< Autopilot altitude bug. */

        int32_t wp_lat, wp_lng;             /*!< Next waypoint latitude and longitude. */
        uint16_t wp_number;                 /*!< Waypoint number. */

        struct vs_raw_acc_mag acc_mag;       /*!< Raw magnetometer and accelerometer readings */
};


/**
 * \brief The Link class is the base class for both Station and Vehicle. It handles the dispatch queue, custom packets, encryption and checksums.
 */

class Link
{
    public:
        Link();
        ~Link();

/**
 * \brief Gets next packet off the dispatch queue.
 * \return The next packet to be sent.
 */
        fl_pack next_off_queue();

/**
 * \brief Pushes a packet onto the dispatch queue. The ID of the packet must be at least 1024 if you want to receive it with on_custom_recv().
 * \param pack The packet to be pushed.
 * \return The ID of the packet
 */
        uint16_t push_custom_pack(fl_pack pack);

/**
 * \brief Checks if a packet is on the dispatch queue.
 * \param id The ID of the packet
 * \return true if the packet is on the dispatch queue, otherwise false.
 */
        bool on_queue(uint16_t id);

/**
 * \brief Removes a packet from the dispatch queue.
 * \param id The ID of the packet to be removed.
 * \return true if a packet was removed, otherwise false.
 */
        bool remove_pack(uint16_t id);

/**
 * \brief Removes all packets from the dispatch queue and frees all associated memory.
 */
        void clear_queue();

/**
 * \brief Returns the number of packets on the dispatch queue.
 */
        int queue_size();

/**
 * \brief This function will decrypt a packet.
 * \param data_in The encrypted data (16 bytes)
 * \param data_out The decrypted data. This must point to an array of at least 16 bytes.
 */
        void decrypt_pack(unsigned char* data_in, unsigned char* data_out);

/**
 * \brief This function is overridden by both Station and Vehicle.
 * At this level it simply processes the packet and if the ID is greater than 1024, it passes the data to on_custom_recv
 * \param pack A pointer to an array of at least 20 bytes that contains a FireLink packet.
 * \return Returns -1 if the checksum is bad, otherwise it returns the packet ID
 */
        virtual int32_t read_pack(unsigned char* pack);

/**
 * \brief To receive packets of your own type, you can override this function.
 * \param id The ID of the packet received. It must be greater than 1024.
 * \param data A pointer to a 12 byte array containing the decrypted packet data.
 * \return Returns false by default. If this function is overridden it should return true if it handles a packet.
 */
        virtual bool on_custom_recv(uint16_t id, unsigned char* data){ return false; }

/**
 * \brief If this is true, packets will be encrypted before dispatch and decrypted when they are read.
 Both the station and vehicle need to have the same IV (see set_iv() ) and encryption key (see set_key() ).
 If they don't have the same IV or key, packets will still be received OK (the checksum will still be good)
 but all data will be gibberish.
 */
        bool encryption_on;

/**
 * \brief Sets the initialisation vector for the AES encryption.
 * \param iv A pointer to a 16 byte array containing the initialisation vector.
 For ideal encryption this should be changed periodically in a pseudorandom pattern.
 If security isn't critical, this can simply be set to a predetermined value (like the key).
 */
        void set_iv(unsigned char* iv);

/**
 * \brief Sets the key to use for encryption.
 * \param key A pointer to a 32 byte array containing the encryption key.
 */
        void set_key(unsigned char* key);

/**
 * \brief Prevents streamed data packets from being transmitted. Disabling streamed data is useful for speeding up large data transfers.
 * \param stream true to turn off streamed data. false to turn it back on.
 */
        void set_disable_stream(bool stream);

/**
 * \brief Checks if streamed data is disabled.
 * \return True if disabled, false if enabled.
 */
        bool get_disable_stream();

/**
 * \brief Generates a two byte CRC-16 checksum.
 * \param data A pointer to a 16 byte array.
 * \return The checksum.
 */
        static int16_t make_checksum(unsigned char* data);

/**
 * \brief Checks a packet for a valid CRC-16 checksum.
 * \param data A pointer to an 18 byte array. Bytes 17 - 18 should contain the checksum.
 * \return True if the checksum is good, false if it is not.
 */
        static bool check_checksum(unsigned char* data);

/**
 * \brief Resets the repeat message suppression. Calling this may be useful if receiving the same msg several times over is harmless
 * or desirable.
 */
        //void reset_rs() { last_msg_id = 1; }

        uint8_t set_bit(uint8_t bte, uint8_t bit, bool on);
        uint8_t read_bit(uint8_t bte, uint8_t n);

        int32_t encode(unsigned char b);

        void set_vd(VehicleData* pvd) { vd = pvd; }
        VehicleData* get_vd() { return vd; }

    protected:
#ifndef NO_ENCRYPTION
        AES aes;
        unsigned char init_vec[16];
#endif
        PackQueue queue;

        uint16_t ack;
        bool skip_queue;

        uint8_t control_vals[12];

        bool no_stream;

        VehicleData* vd;

        char last_msg_data[12];
        uint16_t last_msg_id;

        unsigned char in_buf[60];
        uint8_t in_buf_pos;

};


};

#endif // LINK_H_INCLUDED
