/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include "vehicle.h"

#include <math.h>

namespace FireLink
{


Vehicle::Vehicle(VehicleData* vd)
{
    set_vd(vd);

    outpack_pos = 0;
    mask = ~(mask & 0); //sets all bits to 1

    //except for accelerometer & magnetometer readings
    set_bit(mask, VS_RAW_ACC_MAG, false);

    in_buf_pos = 0;
}

uint16_t Vehicle::push_satellite_report(unsigned char sat_num, int32_t prn,
                                        uint16_t azimuth, uint16_t elevation, uint16_t ss)
{
    fl_pack pack;
    pack.id = VC_SATELLITE_REPORT;

    vm_satellite_report* sr = (vm_satellite_report*)pack.data;
    sr->sat_num = sat_num;
    sr->prn = prn;
    sr->azimuth = azimuth;
    sr->elevation = elevation;
    sr->ss = ss;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_home_coordinates(float lat, float lng, float elevation)
{
    fl_pack pack;
    pack.id = VC_HOME_COORDINATES;

    fm_waypoint_pack* hc = (fm_waypoint_pack*)pack.data;
    hc->lat = roundf(lat * 1000000.0);
    hc->lng = roundf(lng * 1000000.0);
    hc->alt = elevation + 1000;

    return push_custom_pack(pack);
}

uint16_t Vehicle::push_autopilot_status(AUTOPILOT_STATUS as)
{
    fl_pack pack;
    pack.id = VC_AUTOPILOT_STATUS_CHANGE;
    vm_autopilot_status_change* asc = (vm_autopilot_status_change*)pack.data;
    asc->as = as;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_waypoint_report(uint16_t n, float lat, float lng, int32_t alt)
{
    fl_pack pack;
    pack.id = VC_WAYPOINT_REPORT;

    fm_waypoint_pack* wr = (fm_waypoint_pack*)pack.data;
    wr->n = n;
    wr->lat = roundf(lat * 1000000.0);
    wr->lng = roundf(lng * 1000000.0);
    wr->alt = alt+1000;

    return push_custom_pack(pack);
}

uint16_t Vehicle::push_n_waypoints(uint16_t n)
{
    fl_pack pack;
    pack.id = VC_N_WAYPOINTS;
    vm_n_waypoints* nw = (vm_n_waypoints*)pack.data;
    nw->n = n;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_ahrs_offset_report(float w, float x, float y, float z)
{
    fl_pack pack;
    vm_ahrs_quaternion_report* aqr = (vm_ahrs_quaternion_report*)pack.data;
    aqr->w = roundf(w * 10000.0);
    aqr->x = roundf(x * 10000.0);
    aqr->y = roundf(y * 10000.0);
    aqr->z = roundf(z * 10000.0);

    pack.id = VC_AHRS_OFFSET_REPORT;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_ahrs_quaternion_report(float w, float x, float y, float z)
{
    fl_pack pack;
    vm_ahrs_quaternion_report* aqr = (vm_ahrs_quaternion_report*)pack.data;
    aqr->w = roundf(w * 10000.0);
    aqr->x = roundf(x * 10000.0);
    aqr->y = roundf(y * 10000.0);
    aqr->z = roundf(z * 10000.0);

    pack.id = VC_AHRS_QUATERNION_REPORT;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_mag_cal(fm_mag_cal_pack* mc)
{
    fl_pack pack;
    pack.id = VC_MAG_CAL_REPORT;
    memcpy(pack.data, mc, sizeof(fm_mag_cal_pack));
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_accel_cal(float x, float y, float z)
{
    fl_pack pack;
    pack.id = VC_ACC_CAL_REPORT;
    fm_accel_cal_pack* acp = (fm_accel_cal_pack*)pack.data;
    acp->x_bias = roundf(x*10000);
    acp->y_bias = roundf(y*10000);
    acp->z_bias = roundf(z*10000);

    return push_custom_pack(pack);
}

uint16_t Vehicle::push_servo_configuration_report(float scale, int8_t ele_min, int8_t ele_max,
                                                                    int8_t ail_min, int8_t ail_max,
                                                                    int8_t rud_min, int8_t rud_max,
                                                                    int8_t thr_min, int8_t thr_max)

{
    fl_pack pack;
    pack.id = VC_SERVO_CONFIG_REPORT;

    fm_servo_config* scr = (fm_servo_config*)pack.data;
    scr->scale = roundf(scale*100.0);
    scr->ele_min = ele_min;
    scr->ele_max = ele_max;
    scr->ail_min = ail_min;
    scr->ail_max = ail_max;
    scr->rud_min = rud_min;
    scr->rud_max = rud_max;
    scr->thr_min = thr_min;
    scr->thr_max = thr_max;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_pitch_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_PITCH_GAINS_REPORT;
    fm_pid_pack* pp = (fm_pid_pack*)pack.data;
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_roll_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_ROLL_GAINS_REPORT;
    fm_pid_pack* pp = (fm_pid_pack*)pack.data;
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_heading_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_HEADING_GAINS_REPORT;
    fm_pid_pack* pp = (fm_pid_pack*)pack.data;
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_vert_speed_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_VERT_SPEED_GAINS_REPORT;
    fm_pid_pack* pp = reinterpret_cast<fm_pid_pack*>(pack.data);
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_max_climb_rates_report(float climb, float decent)
{
    fl_pack pack;
    pack.id = VC_MAX_CLIMB_REPORT;
    fm_max_climb_pack* mc = reinterpret_cast<fm_max_climb_pack*>(pack.data);
    mc->climb = roundf(climb*10);
    mc->decend = roundf(decent*10);
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_altitude_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_ALT_GAINS_REPORT;
    fm_pid_pack* pp = reinterpret_cast<fm_pid_pack*>(pack.data);
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_path_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_PATH_GAINS_REPORT;
    fm_pid_pack* pp = (fm_pid_pack*)pack.data;
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_at_gains_report(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = VC_AT_GAINS_REPORT;
    fm_pid_pack* pp = (fm_pid_pack*)pack.data;
    pp->kp = kp * 10000.0;
    pp->ki = ki * 10000.0;
    pp->kd = kd * 10000.0;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_mixer_configuration_report(MIXER_CONFIGURATION config, uint8_t mixratio)
{
    fl_pack pack;
    pack.id = VC_MIXER_CONFIG_REPORT;

    vm_mixer_config_report* mcr = (vm_mixer_config_report*)pack.data;
    mcr->config = config;
    mcr->mixratio = mixratio;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_magnetic_variation_report(float mag_var)
{
    fl_pack pack;
    pack.id = VC_MAG_VAR_REPORT;
    vm_mag_var_report* mvr = (vm_mag_var_report*)pack.data;
    mvr->mag_var = roundf(mag_var * 100.0);
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_loiter_settings_report(uint32_t loiter_radius, float correction_strength)
{
    fl_pack pack;
    loiter_settings_pack* lsp = (loiter_settings_pack*)pack.data;
    pack.id = VC_LOITER_SETTINGS_REPORT;

    lsp->loiter_radius = loiter_radius;
    lsp->correction_strength = correction_strength*100;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_turn_coord_ratios_report(float yc, float ptb)
{
    fl_pack pack;
    fm_turn_coord_pack* ycp = (fm_turn_coord_pack*)pack.data;
    pack.id = VC_TURN_COORD_REPORT;
    ycp->yc_ratio = roundf(yc*100);
    ycp->bank_pitch_ratio =roundf(ptb*1000);
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_max_angles_report(float pitch_down, float pitch_up, float roll)
{
    fl_pack pack;
    pack.id = VC_MAX_ANGLES_REPORT;
    fm_max_angles* ma = (fm_max_angles*)pack.data;
    ma->pitch_down = roundf(pitch_down*100);
    ma->pitch_up = roundf(pitch_up*100);
    ma->roll = roundf(roll*100);
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_at_settings_report(float pt, float cs, int cp, int minthr)
{
    fl_pack pack;
    pack.id = VC_AT_SETTINGS_REPORT;
    fm_at_settings* as = (fm_at_settings*)pack.data;
    as->pt = roundf(pt*100);
    as->cs = roundf(cs*100);
    as->cp = cp;
    as->min_thr = minthr;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_control_source_report(CONTROL_SOURCE cs)
{
    fl_pack pack;
    pack.id = VC_CONTROL_SOURCE_REPORT;
    fm_control_source* csp = (fm_control_source*)pack.data;
    csp->control_source = cs;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_battery_mah_report(uint32_t mah)
{
    fl_pack pack;
    pack.id = VC_BATTERY_MAH_REPORT;
    fm_battery_mah* bah = (fm_battery_mah*)pack.data;
    bah->mah = mah;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_geofence_point_report(uint16_t n, float lat, float lng)
{
    fl_pack pack;
    pack.id = VC_GEOPOINT_REPORT;
    fm_waypoint_pack* wp = (fm_waypoint_pack*)pack.data;
    wp->n = n;
    wp->lat = roundf(lat*1000000);
    wp->lng = roundf(lng*1000000);
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_n_geofence_points(uint16_t n)
{
    fl_pack pack;
    pack.id = VC_N_GEOPOINTS;
    vm_n_waypoints* nw = (vm_n_waypoints*)pack.data;
    nw->n = n;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_alarm_actions_report(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem)
{
    fl_pack pack;
    pack.id = VC_ALARM_ACTIONS_REPORT;
    fm_alarm_actions* faa = (fm_alarm_actions*)pack.data;
    faa->low_bat = lb;
    faa->boundary = ba;
    faa->no_gps_lock = gpsa;
    faa->lost_control = lca;
    faa->no_telem = no_telem;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_alarm_activated(ALARM_TYPE alarm)
{
    fl_pack pack;
    pack.id = VC_ALARM_ACTIVATED;
    vm_alarm_activated_pack* ap = (vm_alarm_activated_pack*)pack.data;
    ap->alarm = alarm;
    return push_custom_pack(pack);
}


uint16_t Vehicle::push_max_euler_rates_report(uint16_t pitch, uint16_t roll)
{
    fl_pack pack;
    pack.id = VC_MAX_EULER_RATES_REPORT;
    fm_max_euler_rates* mr = (fm_max_euler_rates*)pack.data;
    mr->pitch = pitch;
    mr->roll = roll;
    return push_custom_pack(pack);
}

uint16_t Vehicle::push_speed_gain_report(float sg)
{
    fl_pack pack;
    pack.id = VC_SPEED_GAIN_REPORT;
    int32_t* dt = (int32_t*)pack.data;
    *dt = (sg*10000);
    return push_custom_pack(pack);
}


uint16_t Vehicle::push_debug_message(char* msg)
{
    if(on_queue(VC_DEBUG_MSG_FLUSH))
        return 0;

    int msglen = 0;
    for(int i = 0; i < 120; i++)
    {
        if(msg[i] == '\0')
            break;
        msglen++;
    }

    //twelve bytes per packet, so divide by 12 and round up to get number of packets
    int npacks = ceil(msglen/12.0);

    for(int i = 0; i < npacks-1; i++)
    {
        fl_pack pack;
        pack.id = VC_DEBUG_MSG+i;
        memcpy(pack.data, msg+(i*12), 12);
        push_custom_pack(pack);
    }

    int remaining_bytes = fmod(msglen,12.0);
    fl_pack pck;
    pck.id = VC_DEBUG_MSG+npacks-1;
    memcpy(pck.data, msg+((npacks-1)*12), remaining_bytes);
    memset(pck.data+remaining_bytes, 0, 12-remaining_bytes);
    push_custom_pack(pck);

    pck.id = VC_DEBUG_MSG_FLUSH;
    push_custom_pack(pck);
    return pck.id;
}


void Vehicle::next_pack(unsigned char* ret)
{
    unsigned char buf[20];
    fl_pack pack;

    if((queue_size() > 0) && ((!skip_queue)||(no_stream)))//if there is a pack queued
    {
        pack = next_off_queue();
        skip_queue = true;
    }
    else //otherwise send streamed data
    {
        skip_queue = false;
        int ctr = 0;
        while(1)
        {
            ctr++;
            if(ctr > 2)
            {
                pack.id = VS_NULL;
                break;
            }

            outpack_pos++;
            if(outpack_pos == VS_NULL)
                outpack_pos = 0;

            if((outpack_pos == VS_INST_1) && (mask & 1 << VS_INST_1)) //make inst 1 packet
            {
                pack.id = VS_INST_1;
                vs_inst_1* vi = (vs_inst_1*)pack.data;
                uint16_t ba = vd->alt + 1000;

                vi->heading = vd->heading;
                vi->pitch = vd->pitch;
                vi->roll = vd->roll;
                vi->baroalt = ba;
                vi->vert_speed = vd->vert_speed;
                vi->air_speed = vd->air_speed;
                break;
            }
            if((outpack_pos == VS_INST_2) && (mask & 1 << VS_INST_2)) //inst 2 packet
            {
                pack.id = VS_INST_2;
                vs_inst_2* vi = (vs_inst_2*)pack.data;

                vi->latitude = vd->latitude;
                vi->longitude = vd->longitude;
                vi->ground_speed = vd->ground_speed;
                vi->sats_used = vd->satellites_used;
                vi->gps_mode = vd->gps_mode;
                break;
            }
            if((outpack_pos == VS_POWER) && (mask & 1 << VS_POWER))
            {
                pack.id = VS_POWER;
                vs_power* vp = (vs_power*)pack.data;

                vp->switches = set_bit(vp->switches, 1, vd->motor_armed);
                vp->switches = set_bit(vp->switches, 2, vd->autothrottle_on);
                vp->switches = set_bit(vp->switches, 3, vd->hil_mode);
                vp->switches = set_bit(vp->switches, 4, vd->has_rc);
                vp->switches = set_bit(vp->switches, 5, vd->inside_boundary);
                vp->switches = set_bit(vp->switches, 6, vd->autolaunch_on);

                vp->milliamps = vd->milliamps;
                vp->power_remaining = vd->power_remaining;
                vp->timer = vd->timer;
                vp->volts = vd->volts;
                vp->temperature = vd->temperature;
                vp->ground_track = vd->ground_track;
                break;
            }
            if((outpack_pos == VS_AP_STATUS) && (mask & 1 << VS_AP_STATUS))
            {
                pack.id = VS_AP_STATUS;
                vs_ap_status* vas = (vs_ap_status*)pack.data;
                vas->ap_alt = vd->ap_alt;
                vas->ap_heading = vd->ap_heading;
                vas->ap_mode = vd->ap_mode;
                vas->ap_pitch = vd->ap_pitch;
                vas->ap_roll = vd->ap_roll;
                break;
            }
            if((outpack_pos == VS_AP_STATUS2) && (mask & 1 << VS_AP_STATUS2))
            {
                pack.id = VS_AP_STATUS2;
                vs_ap_status2* vas = (vs_ap_status2*)pack.data;
                vas->wp_lat = vd->wp_lat;
                vas->wp_lng = vd->wp_lng;
                vas->wp_number = vd->wp_number;
                break;
            }
            if((outpack_pos == VS_RAW_ACC_MAG) && (mask & 1 << VS_RAW_ACC_MAG))
            {
                pack.id = VS_RAW_ACC_MAG;
                vs_raw_acc_mag* ram = (vs_raw_acc_mag*)pack.data;
                memcpy(ram, &vd->acc_mag, sizeof(vs_raw_acc_mag));
                break;
            }
        }
    }

    memcpy(buf, &pack, 14);
    memcpy(buf+14, &ack, 2);

#ifndef NO_ENCRYPTION
    if(encryption_on)
    {
        unsigned char IV[16];
        memcpy(IV, init_vec, 16);
        aes.cbc_encrypt(buf, ret, 1, IV);
    }
    else
    {
        memcpy(ret, buf, 16);
    }
#else
    memcpy(ret, buf, 16);
#endif
    int16_t* pcs = (int16_t*)ret+8;
    *pcs = make_checksum(ret);
    ret[18] = '\n';
    ret[19] = '\0';
}

int32_t Vehicle::read_pack(unsigned char* pack)
{
    unsigned char decrypt_buf[16];
    unsigned char* data;
    uint16_t id;

    if(!check_checksum(pack))
    {
        return -1;
    }

#ifndef NO_ENCRYPTION
    if(encryption_on)
    {
        unsigned char IV[16];
        memcpy(IV, init_vec, 16);
        aes.cbc_decrypt(pack, decrypt_buf, 1, IV);
    }
    else
    {
        memcpy(decrypt_buf, pack, 16);
    }
#else
    memcpy(decrypt_buf, pack, 16);
#endif

    id = *(uint16_t*)decrypt_buf;
    data = decrypt_buf + 2;
    ack = id; //save the ack

    //remove commands from queue if they are ACKed
    uint16_t inack = *(uint16_t*)(decrypt_buf+14);
    if(next_off_queue().id == inack) //if it's on the queue, remove it
        remove_pack(inack);


    //ignore repeated packets
    if((id == last_msg_id) && (id != SC_KEEP_ALIVE)) //don't ignore keep-alive packets
    {
        if(memcmp(last_msg_data, data, 12) == 0)
            return id;
    }
    if(id >= 128)
    {
        last_msg_id = id;
        memcpy(last_msg_data, data, 12);
    }

    if(id == SC_FLUSH)
        return id;

    bool ret = false;
    switch(id)
    {
    case SS_CONTROL: //control packet
    {
        memcpy(control_vals, data, 12);
        ret = true;
    }
    break;

    //we got this far. it wasn't a streaming packet
    case SC_KEEP_ALIVE:
    {
        sm_keep_alive* df = (sm_keep_alive*)data;
        ret = on_keep_alive_recv(df->seconds);
    }
    break;
    case SC_ARM_MOTOR: //arm motor
    {
        sm_arm_motor* am = (sm_arm_motor*)data;
        ret = on_arm_motor_recv(am->arm);
    }
    break;
    case SC_GET_AUTOPILOT_STATUS:
    {
        ret = on_get_autopilot_status();
    }
    break;
    case SC_SET_AP_MODE:
    {
        sm_set_ap_mode* sam;
        sam = (sm_set_ap_mode*)data;
        ret = on_set_ap_mode(sam->mode);
    }
    break;
    case SC_GET_SATELLITE_REPORT:
    {
        ret = on_get_satellite_report(data[0]);
    }
    break;
    case SC_GET_HOME_COORDINATES:
    {
        ret = on_get_home_coordinates();
    }
    break;
    case SC_SET_HOME_COORDINATES:
    {
        fm_waypoint_pack* shc = (fm_waypoint_pack*)data;
        int ha = shc->alt;
        ret = on_set_home_coordinates(shc->lat/1000000.0, shc->lng/1000000.0, ha-1000);
    }
    break;
    case SC_SET_ALTIMETER_SOURCE:
    {
        sm_set_altimeter_source* sas = (sm_set_altimeter_source*)data;
        ret = on_set_altimeter_source(sas->alt_source);
    }
    break;
    case SC_CLEAR_WAYPOINTS:
    {
        ret = on_clear_waypoints();
    }
    break;
    case SC_REMOVE_WAYPOINT:
    {
        sm_remove_waypoint* rw = (sm_remove_waypoint*)data;
        ret = on_remove_waypoint(rw->waypoint);
    }
    break;
    case SC_GET_WAYPOINT:
    {
        sm_get_waypoint* gw = (sm_get_waypoint*)data;
        ret = on_get_waypoint(gw->waypoint);
    }
    break;
    case SC_GET_WAYPOINT2:
    {
        sm_get_waypoint* gw = (sm_get_waypoint*)data;
        ret = on_get_waypoint(gw->waypoint);
    }
    break;
    case SC_GET_N_WAYPOINTS:
    {
        ret = on_get_n_waypoints();
    }
    break;
    case SC_SET_AHRS_INSTALLATION_OFFSET:
    {
        sm_set_ahrs_installation_offset* saio = (sm_set_ahrs_installation_offset*)data;
        ret = on_set_ahrs_installation_offset(
                  saio->w/10000.0, saio->x/10000.0,
                  saio->y/10000.0, saio->z/10000.0);
    }
    break;
    case SC_GET_AHRS_INSTALLATION_OFFSET:
    {
        ret = on_get_ahrs_offset();
    }
    break;
    case SC_GET_AHRS_QUATERNION:
    {
        ret = on_get_ahrs_quaternion();
    }
    break;
    case SC_GET_MAGNETOMETER_CAL:
    {
        ret = on_get_mag_cal();
    }
    break;
    case SC_SET_MAGNETOMETER_CAL:
    {
        ret = on_set_mag_cal((fm_mag_cal_pack*)data);
    }
    break;
    case SC_GET_ACCELEROMETER_CAL:
    {
        ret = on_get_accel_cal();
    }
    break;
    case SC_SET_ACCELEROMETER_CAL:
    {
        fm_accel_cal_pack* ac = (fm_accel_cal_pack*)data;
        ret = on_set_accel_cal(ac->x_bias/10000.0, ac->y_bias/10000.0, ac->z_bias/10000.0);
    }
    break;
    case SC_GET_SERVO_CONFIG:
    {
        ret = on_get_servo_configuration();
    }
    break;
    case SC_SET_SERVO_CONFIG:
    {
        fm_servo_config* ssc = (fm_servo_config*)data;
        ret = on_set_servo_configuration(ssc->scale/100.0, ssc->ele_min, ssc->ele_max,
                                        ssc->ail_min, ssc->ail_max,
                                        ssc->rud_min, ssc->rud_max,
                                        ssc->thr_min, ssc->thr_max);
    }
    break;
    case SC_GET_MIXER_CONFIG:
    {
        ret = on_get_mixer_configuration();
    }
    break;
    case SC_SET_MIXER_CONFIG:
    {
        sm_set_mixer_config* smc = (sm_set_mixer_config*)data;
        ret = on_set_mixer_configuration(smc->config, smc->mixratio);
    }
    break;
    case SC_GET_PITCH_GAINS:
    {
        ret = on_get_pitch_gains();
    }
    break;
    case SC_SET_PITCH_GAINS:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_set_pitch_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_GET_ROLL_GAINS:
    {
        ret = on_get_roll_gains();
    }
    break;
    case SC_SET_ROLL_GAINS:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_set_roll_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_GET_HEADING_GAINS:
    {
        ret = on_get_heading_gains();
    }
    break;
    case SC_SET_HEADING_GAINS:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_set_heading_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_GET_VERT_SPEED_GAINS:
    {
        ret = on_get_vert_speed_gains();
    }
    break;
    case SC_SET_VERT_SPEED_GAINS:
    {
        fm_pid_pack* pp = reinterpret_cast<fm_pid_pack*>(data);
        ret = on_set_vert_speed_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_GET_ALT_GAINS:
    {
        ret = on_get_altitude_gains();
    }
    break;
    case SC_SET_ALT_GAINS:
    {
        fm_pid_pack* pp = reinterpret_cast<fm_pid_pack*>(data);
        ret = on_set_altitude_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_SET_MAX_CLIMB:
    {
        fm_max_climb_pack* mc = reinterpret_cast<fm_max_climb_pack*>(data);
        ret = on_set_max_climb_rates(mc->climb/10.0, mc->decend/10.0);
    }
    break;
    case SC_GET_MAX_CLIMB:
    {
        ret = on_get_max_climb_rates();
    }
    break;
    case SC_GET_PATH_GAINS:
    {
        ret = on_get_path_gains();
    }
    break;
    case SC_SET_PATH_GAINS:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_set_path_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_GET_AT_GAINS:
    {
        ret = on_get_at_gains();
    }
    break;
    case SC_SET_AT_GAINS:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_set_at_gains(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case SC_GET_MAG_VAR:
    {
        ret = on_get_magnetic_variation();
    }
    break;
    case SC_SET_MAG_VAR:
    {
        sm_set_mag_var* smv = (sm_set_mag_var*)data;
        ret = on_set_magnetic_variation(smv->mag_var/100.0);
    }
    break;
    case SC_SET_LOITER_SETTINGS:
    {
        loiter_settings_pack* lsp = (loiter_settings_pack*)data;
        ret = on_set_loiter_settings(lsp->loiter_radius, lsp->correction_strength/100.0);
    }
    break;
    case SC_GET_LOITER_SETTINGS:
    {
        ret = on_get_loiter_settings();
    }
    break;
    case SC_GET_TURN_COORD_RATIOS:
    {
        ret = on_get_turn_coord_ratios();
    }
    break;
    case SC_SET_TURN_COORD_RATIOS:
    {
        fm_turn_coord_pack* ycp = (fm_turn_coord_pack*)data;
        ret = on_set_turn_coord_ratios(ycp->yc_ratio/100.0, ycp->bank_pitch_ratio/1000.0);
    }
    break;
    case SC_GET_MAX_ANGLES:
    {
        ret = on_get_max_angles();
    }
    break;
    case SC_SET_MAX_ANGLES:
    {
        fm_max_angles* ma = (fm_max_angles*)data;
        ret = on_set_max_angles(ma->pitch_down/100.0, ma->pitch_up/100.0, ma->roll/100);
    }
    break;
    case SC_GET_AT_SETTINGS:
    {
        ret = on_get_at_settings();
    }
    break;
    case SC_SET_AT_SETTINGS:
    {
        fm_at_settings* as = (fm_at_settings*)data;
        ret = on_set_at_settings(as->pt/100.0, as->cs/100.0, as->cp, as->min_thr);
    }
    break;
    case SC_HIL_MODE:
    {
        sm_hil_mode* sh = (sm_hil_mode*)data;
        ret = on_set_hil_mode(sh->hm);
    }
    break;
    case SC_AUTOTHROTTLE:
    {
        sm_autothrottle* sa = (sm_autothrottle*)data;
        ret = on_autothrottle_recv(sa->on);
    }
    break;
    case SC_AUTOLAUNCH:
    {
        sm_autothrottle* sa = (sm_autothrottle*)data;
        ret = on_autolaunch_recv(sa->on);
    }
    break;
    case SC_GET_CONTROL_SOURCE:
    {
        ret = on_get_control_source();
    }
    break;
    case SC_SET_CONTROL_SOURCE:
    {
        fm_control_source* csp = (fm_control_source*)data;
        ret = on_set_control_source(csp->control_source);
    }
    break;
    case SC_SET_BATTERY_MAH:
    {
        fm_battery_mah* bah = (fm_battery_mah*)data;
        ret = on_set_battery_mah(bah->mah);
    }
    break;
    case SC_GET_BATTERY_MAH:
    {
        ret = on_get_battery_mah();
    }
    break;
    case SC_ADD_WAYPOINT:
    {
        awp:
        fm_waypoint_pack* wa = (fm_waypoint_pack*)data;
        ret = on_add_waypoint(wa->n, wa->lat/1000000.0, wa->lng/1000000.0, wa->alt-1000);
    }
    break;
    case SC_ADD_WAYPOINT2:
    {
        goto awp;
    }
    break;
    case SC_ADD_GEOPOINT:
    {
        add_gp:
        fm_waypoint_pack* wa = (fm_waypoint_pack*)data;
        ret = on_add_geofence_point(wa->n, wa->lat/1000000.0, wa->lng/1000000.0);
    }
    break;
    case SC_ADD_GEOPOINT2:
    {
        goto add_gp;
    }
    break;
    case SC_GET_GEOPOINT:
    {
        req_gp:
        sm_get_waypoint* wp = (sm_get_waypoint*)data;
        ret = on_get_geofence_point(wp->waypoint);
    }
    break;
    case SC_GET_GEOPOINT2:
    {
        goto req_gp;
    }
    break;
    case SC_GET_N_GEOPOINTS:
    {
        ret = on_get_n_geofence_points();
    }
    break;
    case SC_REMOVE_GEOPOINT:
    {
        sm_remove_waypoint* rm = (sm_remove_waypoint*)data;
        ret = on_remove_geopoint(rm->waypoint);
    }
    break;
    case SC_CLEAR_GEOPOINTS:
    {
        ret = on_clear_geopoints();
    }
    break;
    case SC_SET_ALARM_ACTIONS:
    {
        fm_alarm_actions* faa = (fm_alarm_actions*)data;
        ret = on_set_alarm_actions(faa->low_bat, faa->boundary, faa->no_gps_lock, faa->lost_control, faa->no_telem);
    }
    break;
    case SC_GET_ALARM_ACTIONS:
    {
        ret = on_get_alarm_actions();
    }
    break;
    case SC_SET_MAX_EULER_RATES:
    {
        fm_max_euler_rates* mr = (fm_max_euler_rates*)data;
        ret = on_set_max_euler_rates(mr->pitch, mr->roll);
    }
    break;
    case SC_GET_MAX_EULER_RATES:
    {
        ret = on_get_max_euler_rates();
    }
    break;
    case SC_SET_SPEED_GAIN:
    {
        int32_t* sg = (int32_t*)data;
        ret = on_set_speed_gain(*sg/10000.0);
    }
    break;
    case SC_GET_SPEED_GAIN:
    {
        ret = on_get_speed_gain();
    }
    break;
    case SC_SET_STREAM_MASK:
    {
        memcpy(&mask, data, sizeof(uint64_t));
        ret = true;
    }
    break;
    case SC_FLIGHT_COMMAND:
    {
        FLIGHT_COMMAND* fc = (FLIGHT_COMMAND*)data;
        ret = on_get_flight_command(*fc);
    }
    break;
    case SC_SET_PNR:
    {
        fm_select_pnr_pack* pnr = (fm_select_pnr_pack*)data;
        ret = on_set_selected_pitch_and_roll(pnr->pitch, pnr->roll);
    }
    break;
    case SC_SET_HNA:
    {
        fm_select_hna_pack* hna = (fm_select_hna_pack*)data;
        ret = on_set_selected_heading_and_alt(hna->heading, hna->alt);
    }
    break;
    }

    if(!ret)
        Link::read_pack(pack);
    return id;
}


void Vehicle::stream_pack_mask(uint16_t id, bool enabled)
{
    mask = (mask & ~(1<<id)) | (enabled << id);
}


unsigned char* Vehicle::get_control_vals()
{
    return control_vals;
}



};
