/**
 * \file vehicle.h
 * \brief FireLink Vehicle class. This class handles all of the FireLink communication needs
 * for a UAV or ROV. Vehicles should receive messages and handle them inside the control loop.
 * Received messages must be received by overriding the handler functions ( bool on_*****() ).
 */

/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef VEHICLE_H_INCLUDED
#define VEHICLE_H_INCLUDED

#include "link.h"

namespace FireLink
{

/**
 * \brief The Vehicle class.
 */
class Vehicle : public Link
{
    public:
        Vehicle(VehicleData* vd);
        ~Vehicle() {};

/**
 * \brief Override this function to receive keep-alive packets.
 * When this packet is received, the vehicle must transmit for the specified number of seconds.
 * If this time expires and the vehicle isn't receiving valid FireLink packets then it should not transmit.
 * \param seconds The number of seconds to transmit for.
 * \return Return true the packet has been received ok.
 */
        virtual bool on_keep_alive_recv(uint32_t seconds) { return false; }

/**
 * \brief Override this function to receive arm motor packets.
 * When the motor is disarmed, the vehicle must disable the motor and prevent it from running under all circumstances.
 * \param arm True is the motor armed, false to disarm
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_arm_motor_recv(bool arm) { return false; }

/**
 * \brief Override this function to receive set autopilot mode packets.
 * \param mode The autopilot mode selected by the station.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_ap_mode(AUTOPILOT_MODE mode) { return false; }

/**
 * \brief Override this function to receive GETs for the autopilot status.
 * When this packet is received, the vehicle should respond by pushing a VC_AUTOPILOT_STATUS_CHANGE packet onto the dispatch queue.
 * Refer push_autopilot_status().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_autopilot_status() { return false; }

/**
 * \brief Override this function to receive GETs for a satellite report.
 * When this packet is received, the vehicle should respond by pushing a VC_SATELLITE_REPORT onto the dispatch queue.
 * Refer push push_satellite_report().
 * \param sat_number The number of the satellite.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_satellite_report(unsigned char sat_number) { return false; }

/**
 * \brief Override this function to receive GETs for home coordinates.
 * When this packet is received, the vehicle should respond by pushing a VC_HOME_COORDINATES packet.
 * Refer push_home_coordinates().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_home_coordinates() { return false; }

/**
 * \brief Override this function to receive set home coordinates packets.
 * These coordinates should be used for return-to-base mode.
 * \param lat Latitude in degrees
 * \param lon Longitude in degrees
 * \param alt Altitude above sea level in feet.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_home_coordinates(float lat, float lon, float alt) { return false; }

/**
 * \brief Override this function to receive set altimeter source packets.
 * If there are multiple altitude sources (baro pressure, GPS) this packet can be used to switch between them.
 * \param as Altimeter source
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_altimeter_source(ALTIMETER_SOURCE as) { return false; }

/**
 * \brief Override this function. When this function is called, all waypoints must be removed from the waypoint list.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_clear_waypoints() { return false; }

/**
 * \brief Override this function. When this packet is received a new waypoint should be added to the waypoint list.
 * \param n Waypoint number.
 * \param lat Latitude in degrees.
 * \param lon Longitude in degrees.
 * \param alt Altitude in feet above sea level.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_add_waypoint(uint16_t n, float lat, float lon, int32_t alt) { return false; }

/**
 * \brief Override this function. When this packet is received a waypoint must be removed from the waypoint list.
 * \param n Waypoint number to remove.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_remove_waypoint(uint16_t n) { return false; }

/**
 * \brief Override this function to receive GETs for waypoint information.
 * The vehicle must push a VC_WAYPOINT_REPORT packet. Refer push_waypoint_report().
 * \param n Waypoint number that the station wants to know about.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_waypoint(uint16_t n) { return false; }

/**
 * \brief Override this function to receive GETs for the number of waypoints in the waypoint list
 * When this packet is received the vehicle must push a VC_N_WAYPOINTS packet. Refer push_n_waypoints.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_n_waypoints() { return false; }

/**
 * \brief Override this function to receive AHRS installation offsets.
 * When this packet is received, the AHRS installation offset must be set to this.
 * w, x, y and z should be represent a unit quaternion.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_ahrs_installation_offset(float w, float x, float y, float z) { return false; }

/**
 * \brief Override this function to receive GETs for the AHRS orientation.
 * The vehicle should respond by pushing a VC_AHRS_QUATERNION_REPORT packet. Refer push_ahrs_quaternion_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_ahrs_quaternion() { return false; }

/**
 * \brief Override this function to receive GETs for the AHRS offset.
 * The vehicle should respond by pushing a VC_AHRS_OFFSET_REPORT packet. Refer push_ahrs_offset_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_ahrs_offset() { return false; }

/**
 * \brief Override this function to receive magnetometer calibration data from the station.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_mag_cal(fm_mag_cal_pack* mc) { return false; }

/**
 * \brief Override this function to receive GETs for magnetometer calibration data.
 * The vehicle must repond by pushing a VC_MAG_CAL_REPORT packet onto the dispatch queue. Refer push_mag_cal().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_mag_cal() { return false; }

        virtual bool on_set_accel_cal(float x, float y, float z) { return false; }

        virtual bool on_get_accel_cal() { return false; }

        virtual bool on_set_selected_heading_and_alt(int16_t heading, int32_t alt) { return false; }

        virtual bool on_set_selected_pitch_and_roll(int16_t pitch, int16_t roll) { return false; }

/**
 * \brief Override this function to receive GETs for servo configuration.
 * This vehicle must respond by pushing a VC_SERVO_CONFIG_REPORT packet onto the dispatch queue. Refer push_servo_configuration_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_servo_configuration() { return false; }

/**
 * \brief Override this function to receive servo configuration data from the Station.
 * \param scale The RC signal scale.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_servo_configuration(float scale, int8_t ele_min, int8_t ele_max,
                                                    int8_t ail_min, int8_t ail_max,
                                                    int8_t rud_min, int8_t rud_max,
                                                    int8_t thr_min, int8_t thr_max) { return false; }

/**
 * \brief Override this function to receive GETs for mixer configuration.
 * The vehicle must respond by pushing a VC_MIXER_CONFIG_REPORT packet onto the dispatch queue. Refer push_mixer_configuration_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_mixer_configuration() { return false; }

/**
 * \brief Override this function to receive mixer configuration data from the Station.
 * \param config The mixer configuration type.
 * \param mixratio The mix ratio from 0 - 100 where less means more elevator.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_mixer_configuration(MIXER_CONFIGURATION config, uint8_t mixratio) { return false; }


/**
 * \brief Override this function to receive GETs for pitch PID gains.
 * The vehicle must reply by pushing a VC_PITCH_GAINS_REPORT packet. Refer push_pitch_gains_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_pitch_gains() { return false; }

/**
 * \brief Override this function to receive pitch PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_pitch_gains(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to receive GETs for roll PID gains.
 * The vehicle must reply by pushing a VC_ROLL_GAINS_REPORT packet. Refer push_roll_gains_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_roll_gains() { return false; }

/**
 * \brief Override this function to receive roll PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_roll_gains(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to receive GETs for heading PID gains.
 * The vehicle must reply by pushing a VC_HEADING_GAINS_REPORT packet. Refer push_heading_gains_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_heading_gains() { return false; }

/**
 * \brief Override this function to receive heading PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_heading_gains(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to receive gets for vertical speed PID gains.
 * Must reply with push_vert_speed_gains_report().
 * \return Return true.
 */
        virtual bool on_get_vert_speed_gains() { return false; }

/**
 * \brief Override this function to receive vertical speed PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_vert_speed_gains(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to receive max climb/decend rates.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_max_climb_rates(float climb, float decent) { return false; }

/**
 * \brief Override this function to receive gets for max climb/decent rates.
 * The vehicle must reply with push_max_climb_rates_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_max_climb_rates() { return false; }

/**
 * \brief Override this function to receive GETs for altitude PID gains.
 * The vehicle must reply by pushing a VC_ALT_GAINS_REPORT packet. Refer push_altitude_gains_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_altitude_gains() { return false; }

/**
 * \brief Override this function to receive altitude PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_altitude_gains(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to receive GETs for path controller PID gains.
 * The vehicle must reply by pushing a VC_PATH_GAINS_REPORT packet. Refer push_path_gains_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_path_gains() { return false; }

/**
 * \brief Override this function to receive path PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_path_gains(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to receive GETs for autothrottle PID gains.
 * The vehicle must reply by pushing a VC_AT_GAINS_REPORT packet. Refer push_at_gains_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_at_gains() { return false; }

/**
 * \brief Override this function to set autothrottle PID gains.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_at_gains(float kp, float ki, float kd) { return false; }


/**
 * \brief Override this function to get GETs for autothrottle settings
 * \return Return true.
 */
        virtual bool on_get_at_settings() { return false; }

/**
 * \brief Override this function to receive autothrottle settings
 * \param pt Pitch throttle coupling
 * \param cs Cruise speed in km\h
 * \param cp Cruise power setting
 * \return Return true.
 */
        virtual bool on_set_at_settings(float pt, float cs, int cp, int minthr) { return false; }

/**
 * \brief Override this function to receive autothrottle on/off packets.
 * \param on True to turn the autothrottle on, otherwise false.
 * \return Return true
 */
        virtual bool on_autothrottle_recv(bool on) { return false; }

/**
 * \brief Override this function to receive autolaunch on/off packets.
 * \param on True to turn the autothrottle on, otherwise false.
 * \return Return true
 */
        virtual bool on_autolaunch_recv(bool on) { return false; }

/**
 * \brief Override this function to receive GETs for the control source.
 * The vehicle must respond by pushing a VC_CONTROL_SOURCE_REPORT packet. Refer push_control_source_report().
 * \return Return true.
 */
        virtual bool on_get_control_source() { return false; }

/**
 * \brief Override this function to set the control source.
 * \param cs FireLink can be used to transmit control data, but the vehicle might be capable of using RC receivers as well.
 * Use this packet to switch between them.
 * \return Return true.
 */
        virtual bool on_set_control_source(CONTROL_SOURCE cs) { return false; }

/**
 * \brief Override this function to receive GETs for magnetic variation.
 * The vehicle must respond by pushing a VC_MAG_VAR_REPORT. Refer push_mag_var_report().
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_magnetic_variation() { return false; }

/**
 * \brief Override this function to receive magnetic variation data from the Station.
 * \param mag_var Magnetic variation to use.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_magnetic_variation(float mag_var) { return false; }


/**
 * \brief Override this function to receive GETs for loiter settings.
 * The vehicle must repond by pushing a VC_LOITER_SETTINGS_REPORT. Refer push_loiter_settings_report().
 */
        virtual bool on_get_loiter_settings() { return false; }
/**
 * \brief Override this function to receive loiter settings from the Station.
 * \param loiter_radius The radius of the circle that the vehicle will loiter around.
 * \param correction_strength This is multiplied by the difference between the distance to the loiter point and the loiter radius.
 * The result is a correction factor that determines how hard the vehicle should turn in to keep flying on the loiter radius.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_loiter_settings(uint32_t loiter_radius, float correction_strength) { return false; }

/**
 * \brief Override this function to receive GETs for turn coordination ratios.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_turn_coord_ratios() { return false; }

/**
 * \brief Override this function to set the yaw coupling ratio.
 * \param yc The new yaw coupling ratio.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_turn_coord_ratios(float yc, float ptb) { return false; }

/**
 * \brief Override this function to receive GETs for max angles. Respond with a push_max_angles_report()
 VC_MAX_ANGLES_REPORT.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_get_max_angles() { return false; }

/**
 * \brief Override this function to set the maximum angles
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_max_angles(float pitch_down, float pitch_up, float roll) { return false; }

/**
 * \brief Sets the vehicle into testing mode.
 * \return Return true if the packet has been received ok.
 */
        virtual bool on_set_hil_mode(bool on) { return false; }

/**
 * \brief Sets the battery mAH.
 * \return Return true.
 */
        virtual bool on_set_battery_mah(uint32_t mah) { return false; }

/**
 * \brief Override this to receive GETs for the battery capacity.
 * \return Return true.
 */
        virtual bool on_get_battery_mah() { return false; }

/**
 * \brief Adds a geofence point.
 * \param n Geofence point number.
 * \param lat Latitude of point.
 * \param lng Longitude of point.
 * \return Return true.
 */
        virtual bool on_add_geofence_point(uint16_t n, float lat, float lng) { return false; }

/**
 * \brief Override this to GET the number of geofence points.
 * \return Return true.
 */
        virtual bool on_get_n_geofence_points() { return false; }

/**
 * \brief Override this to receive GETs for geofence points.
 * \param n Geofence point number.
 * \return Return true.
 */
        virtual bool on_get_geofence_point(uint16_t n) { return false; }

/**
 * \brief Override this to receive remove geopoint packets.
 * \param n The geofence point number to remove.
 * \return Return true.
 */
        virtual bool on_remove_geopoint(uint16_t n) { return false; }

/**
 * \brief When this packet is received, all geopoints must be removed.
 * \return Return true.
 */
        virtual bool on_clear_geopoints() { return false; }

/**
 * \brief Sets the action to be carried out when a particular alarm is activated.
 * \return Return true.
 */
        virtual bool on_set_alarm_actions(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem) { return false; }

/**
 * \brief The station wants to know what the alarm actions are.
 * \return Return true.
 */
        virtual bool on_get_alarm_actions() { return false; }


        virtual bool on_set_max_euler_rates(uint16_t pitch, uint16_t roll) { return false; }

        virtual bool on_get_max_euler_rates() { return false; }

        virtual bool on_set_speed_gain(float sg) { return false; }

        virtual bool on_get_speed_gain() { return false; }

        virtual bool on_get_flight_command(FLIGHT_COMMAND fc) { return false; }

/**
 * \brief Pushes a satellite report packet onto the dispatch queue.
 * \param sat_num Satellite number being reported.
 * \param int32_t Pseudo-random noise.
 * \param azimuth Satellite azimuth
 * \param elevation Satellite elevation
 * \param ss Signal to nosie ratio. Zero if the satellite isn't in use.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_satellite_report(unsigned char sat_num, int32_t prn,
                        uint16_t azimuth, uint16_t elevation, uint16_t ss);


/**
 * \brief Pushes a home coordinates report packet onto the queue.
 * \param lat Latitude in degrees.
 * \param lng Longitude in degrees.
 * \param elevation Elevation above sea level in feet.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_home_coordinates(float lat, float lng, float elevation);

/**
 * \brief Pushes a autopilot status change packet onto the queue.
 * \param as The autopilot status.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_autopilot_status(AUTOPILOT_STATUS as);

/**
 * \brief Pushes a waypoint report packet onto the queue.
 * \param n Waypoint number.
 * \param lat Latitude in degrees.
 * \param lng Longitude in degrees.
 * \param alt Altitude above sea level in feet.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_waypoint_report(uint16_t n, float lat, float lng, int32_t alt);

/**
 * \brief Pushes a packet that reports the number of waypoints uploaded.
 * \param n Number of waypoints in waypoint list.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_n_waypoints(uint16_t n);

/**
 * \brief Pushes a packet that reports the installation offset of the AHRS.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_ahrs_offset_report(float w, float x, float y, float z);
/**
 * \brief Pushes a quaternion that represents the orientation of the AHRS.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_ahrs_quaternion_report(float w, float x, float y, float z);

/**
 * \brief Pushes the mag cal data onto the queue.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_mag_cal(fm_mag_cal_pack* mc);

        uint16_t push_accel_cal(float x, float y, float z);

/**
 * \brief Pushes a servo configuration message onto the queue.
 * \param scale A floating point number that scales
 * \param max_throw An integer that limits the amount of travel that the servos
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_servo_configuration_report(float scale, int8_t ele_min, int8_t ele_max,
                                                                    int8_t ail_min, int8_t ail_max,
                                                                    int8_t rud_min, int8_t rud_max,
                                                                    int8_t thr_min, int8_t thr_max);

/**
 * \brief Sends the pitch controller gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_pitch_gains_report(float kp, float ki, float kd);

/**
 * \brief Sends the roll PID controller gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_roll_gains_report(float kp, float ki, float kd);

/**
 * \brief Sends the heading PID controller gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_heading_gains_report(float kp, float ki, float kd);


/**
 * \brief Sends the vertical speed PID gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_vert_speed_gains_report(float kp, float ki, float kd);

/**
 * \brief Sends the maximum climb/decent rates to the station.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_max_climb_rates_report(float climb, float decent);

/**
 * \brief Sends the altitude PID controller gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_altitude_gains_report(float kp, float ki, float kd);

/**
 * \brief Sends the path PID controller gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_path_gains_report(float kp, float ki, float kd);

/**
 * \brief Sends the autothrottle PID controller gains.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_at_gains_report(float kp, float ki, float kd);

/**
 * \brief Pushes the mixer configuration onto the queue.
 * \param config The mixer configuration.
 * \param mixratio The mixer ratio from 0 - 100 where 0 means more elevator.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_mixer_configuration_report(MIXER_CONFIGURATION config, uint8_t mixratio);

/**
 * \brief Pushes a magnetic variation report onto the queue.
 * \param mag_var Magnetic variation.
 * \return Returns the ID of the packet being pushed onto the queue.
 */
        uint16_t push_magnetic_variation_report(float mag_var);

/**
 * \brief Pushes the loiter radius and correction strength onto the queue.
 * \param loiter_radius The radius of the loiter circle.
 * \param correction_strength The correction multiplier that scales the loiter distance error.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_loiter_settings_report(uint32_t loiter_radius, float correction_strength);

/**
 * \brief Pushes a turn coord ratio packet onto the queue.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_turn_coord_ratios_report(float yc, float ptb);

/**
 * \brief Pushes the maximum angles.
 * \return Returns the ID of the packet being pushed.
 */
        uint16_t push_max_angles_report(float pitch_down, float pitch_up, float roll);

/**
 * \brief Pushes the autothrottle settings.
 * \return Returns the ID of the packet.
 */
        uint16_t push_at_settings_report(float pt, float cs, int cp, int minthr);

/**
 * \brief Pushes a control source report packet.
 * \return Returns the ID of the packet.
 */
        uint16_t push_control_source_report(CONTROL_SOURCE cs);

/**
 * \brief Reports th battery capacity that was previously set.
 * \return Returns the ID of the packet.
 */
        uint16_t push_battery_mah_report(uint32_t mah);

/**
 * \brief Sends a geofence point to the station.
 * \return Returns the ID of the packet.
 */
        uint16_t push_geofence_point_report(uint16_t n, float lat, float lng);


/**
 * \brief Sends the number of geofence points to the station.
 * \return Returns the ID of the packet.
 */
        uint16_t push_n_geofence_points(uint16_t n);


        uint16_t push_alarm_actions_report(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem);

        uint16_t push_alarm_activated(ALARM_TYPE alarm);


        uint16_t push_max_euler_rates_report(uint16_t pitch, uint16_t roll);

        uint16_t push_speed_gain_report(float speed_gain);

        //msg can not be more than 120bytes
        uint16_t push_debug_message(char* msg);

/**
 * \brief Generates the next packet to be dispatched. next_pack gets the next fl_pack to be sent,
 * sets the ACK bytes, encrypts the packet and appends a checksum and packet ending ( new-line and null ).
 * The result is a 20 byte array.
 * \param buf The array for the packet data to be stored.
 */
        void next_pack(unsigned char* buf);

/**
 * \brief Checks the checksum, decrypts the packet, processes the data and calls the appropriate handler.
 * \param pack An array containing the received packet.
 * \return True is the packet was read OK, false if not.
 */
        int32_t read_pack(unsigned char* pack);

/**
 * \brief The only data that is streamed to the vehicle is 12 bytes of control data.
 * Typically this could be used to drive up to 12 servos with one byte resolution (256 possible positions).
 * \return A pointer to the array containing control data.
 */
        unsigned char* get_control_vals();

/**
 * \brief Use this to enable/disable a streamed packet type.
 * \param id packet ID
 * \param enabled true to enable packet type, false to disable.
 */
        void stream_pack_mask(uint16_t id, bool enabled);

/**
 * \brief Returns a pointer to the vehicle data. vd is inherited as a protected member from the Link class and can also be used directly.
 * \return A pointer to the vehicle data.
 */
        VehicleData* get_vehicle_data() { return vd; }
    protected:
        int32_t outpack_pos;
        uint64_t mask;


};

};


#endif // VEHICLE_H_INCLUDED
