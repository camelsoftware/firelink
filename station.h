/**
 * \file station.h
 * \brief FireLink Station class. The station class is used to control and communication with a vehicle.
 * Stations can handle vehicle messages by overriding the virtual on_* packet handling functions. Packets
 * that are not handled by the override handlers are placed on the inbound packet queue for processing
 * at later time. This is approach is generally more convenient for multithreaded GUI applications.
 */

/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef STATION_H_INCLUDED
#define STATION_H_INCLUDED

#include <math.h>
#include "link.h"

namespace FireLink
{

/**
 * \brief The Station class
 */
class Station : public Link
{
    public:
        Station();
        ~Station();

/**
 * \brief Override this function to handle satellite reports from the vehicle.
 * \param sat_num Satellite number
 * \param prn Pseudorandom noise
 * \param azimuth Satellite azimuth
 * \param elevation Satellite elevation
 * \param ss Satellite signal to noise ratio. This is zero if the satellite isn't in use.
 * \return Return true if you do not want this packet to go onto the inbound queue.
 */
        virtual bool on_satellite_report(unsigned char sat_num, int prn,
                        uint16_t azimuth, uint16_t elevation, uint16_t ss) { return false; }

/**
 * \brief Override this function to handle vehicle home coordinate reports.
 * \param lat Latitude in degrees as a floating point number.
 * \param lon Longitude in degrees as a floating point number.
 * \param ele elevation in feet.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_home_coordinates_report(float lat, float lon, float ele) {return false; }

/**
 * \brief Override this function to handle autopilot status changes.
 * \param as The new autopilot status
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_autopilot_status_change(AUTOPILOT_STATUS as) { return false; }

/**
 * \brief Override this function to handle waypoint reports.
 * \param n Waypoint number.
 * \param lat Waypoint latitude in degrees as a floating point number.
 * \param lng Waypoint longitude in degrees as a floating point number.
 * \param alt Waypoint altitude in feet above sea level.
 * \return Return true to prevent this packet from going to the inbound queue.
 */
        virtual bool on_waypoint_reported(uint16_t n, float lat, float lng, int alt) { return false; }

/**
 * \brief Override this function to handle number of waypoints reported packets.
 * \param n Number of waypoints uploaded to vehicle.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_n_waypoints_reported(uint16_t n) { return false; }

/**
 * \brief Override this function to handle ahrs offset report packets. Paramters are values for a unit quaternion.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_ahrs_offset_reported(float w, float x, float y, float z) { return false; }

/**
 * \brief Override this function to handle ahrs quaternion report packets. Parameters are values for a unit quaternion.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_ahrs_quaternion_reported(float w, float x, float y, float z) { return false; }


/**
 * \brief Override this function to handle mag cal report packets.
 * \param magcal A pointer to an array of 6 integers that represent min/max readings for each magnetometer axis.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_mag_cal_reported(fm_mag_cal_pack* magcal) { return false; }

        virtual bool on_acc_cal_reported(float x, float y, float z) { return false; }

/**
 * \brief Override this function to handle servo configuration report packets.
 * \param scale RC signal scale
 * \param max_throw Maximum servo throw
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_servo_configuration_reported(float scale, int8_t ele_min, int8_t ele_max,
                                                                    int8_t ail_min, int8_t ail_max,
                                                                    int8_t rud_min, int8_t rud_max,
                                                                    int8_t thr_min, int8_t thr_max) { return false; }

/**
 * \brief Override this function to handle pitch controller gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_pitch_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle roll controller gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_roll_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle heading controller gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_heading_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle vert speed controller gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_vert_speed_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle alt controller gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_altitude_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle max climb/decent rate reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_max_climb_reported(float max_climb, float max_decent) { return false; }

/**
 * \brief Override this function to handle path following controller gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_path_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle autothrottle gains reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_at_gains_reported(float kp, float ki, float kd) { return false; }

/**
 * \brief Override this function to handle mixer configuration report packets.
 * \param config Mixer configuration.
 * \param mixratio Mix ratio from 0 - 100 where 0 is more elevator.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_mixer_configuration_reported(MIXER_CONFIGURATION config, uint8_t mixratio) { return false; }

/**
 * \brief Override this function to handle mangnetic variation report packets.
 * \param mag_var Magnetic variation.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_magnetic_variation_reported(float mag_var) { return false; }

/**
 * \brief Override this function to handle loiter setting reports.
 * \param loiter_radius The radius of the loiter circle.
 * \param correction_strength Correction strength scales the distance error when loitering.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_loiter_settings_reported(uint32_t loiter_radius, float correction_strength) { return false; }


/**
 * \brief Override this function to handle turn coordination ratio reports.
 * \param yc Yaw coupling ratio.
 * \param ptb Pitch up to bank ratio.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_turn_coord_ratios_reported(float yc, float ptb) { return false; }

/**
 * \brief Override this function to handle max angle reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_max_angles_reported(float pitch_down, float pitch_up, float roll) { return false; }

/**
 * \brief Override this function to handle autothrottle settings reports.
 * \param pt Pitch to throttle coupling
 * \param cs Cruise speed in km/h
 * \param cp Power setting required to maintain cruise speed.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_at_settings_reported(float pt, float cs, int cp, int min_thr) { return false; }

/**
 * \brief Override this function to handle control source reports.
 * \param cs The source of control inputs to the vehicle.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_control_source_reported(CONTROL_SOURCE cs) { return false; }

/**
 * \brief Override this function to handle battery capacity reports.
 * \param mah Total capacity/initial charge of the battery fitted to the vehicle.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_battery_mah_reported(uint32_t mah) { return false; }


/**
 * \brief Override this function to receive geofence point reports.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_geofence_point_reported(uint16_t n, float lat, float lng) { return false; }

/**
 * \brief Override this function to receive the number of geofence points.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_n_geofence_points_reported(uint16_t n) { return false; }

/**
 * \brief Override this to get alarm action reports from the vehicle.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_alarm_actions_reported(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem) { return false; }

/**
 * \brief This function is called when the vehicle sends an alarm packet.
 * \return Return true to prevent this packet from going onto the inbound queue.
 */
        virtual bool on_alarm_activated(ALARM_TYPE alarm) { return false; }


        virtual bool on_max_euler_rates_reported(float pitch, float roll) { return false; }

        virtual bool on_speed_gain_reported(float sg) { return false; }

        virtual bool on_debug_msg_received(char* msg) { return false; }


/**
 * \brief Pushes a keep-alive packet onto the dispatch queue. This will force the vehicle to continue transmitting for a specified number of seconds.
 * Vehicles should reply for this number of seconds after receiving a valid packet. This is a power saving feature. Refer sm_keep_alive.
 * \param seconds Number of seconds to continue transmitting.
 * \return The ID of the packet pushed onto the queue.
 */
		uint16_t push_keep_alive_pack(uint32_t seconds);

/**
 * \brief Pushes an arm motor packet onto the dispatch queue. This will arm or disarm the motor. Disarming the motor should make it safe to handle.
 * Note: It is the responsiblity of the vehicle designer to ensure that a disarmed motor will NOT OPERATING under any circumstance. Refer sm_arm_motor.
 * \param arm True to arm motor, false to disarm it.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_arm_motor_pack(bool arm);

/**
 * \brief Pushes a set AP mode packet onto the queue. This is used to switch autopilot modes. Refer sm_set_ap_mode.
 * \param mode The mode to switch to.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_ap_mode_pack(AUTOPILOT_MODE mode);

/**
 * \brief Pushes a GET autopilot status packet onto the queue. The vehicle will reply with VC_AUTOPILOT_STATUS_CHANGE. Refer vm_autopilot_status_change.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_autopilot_status();

/**
 * \brief Pushes a GET for a report on a satellite. Refer sm_GET_satellite_report.
 * \param sat_number The number of the satellite. This can be from 0 - VehicleData::satellites_used.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_satellite_report(unsigned char sat_number);

/**
 * \brief Pushes a GET for the vehicles home coordinates. Vehicle will reply with VC_HOME_COORDINATES. Refer vm_home_coordinates.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_home_coordinates();

/**
 * \brief Pushes a set home coordinates packet onto the dispatch queue. Refer sm_set_home_coordinates.
 * \param lat Latitude in degrees.
 * \param lon Longitude in degrees.
 * \param ele Elevation in feet above sea level.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_home_coordinates(float lat, float lon, float ele);

/**
 * \brief Pushes a packet to switch the altitude source. Refer sm_set_altimeter_source.
 * \param as Altitude source
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_alt_source(ALTIMETER_SOURCE as);

/**
 * \brief Pushes a packet to remove all waypoints uploaded to the vehicle.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_clear_waypoints();

/**
 * \brief Pushes a packet to add a waypoint to the vehicles waypoint list. Refer sm_add_waypoint.
 * \param n Waypoint number
 * \param lat Latitude in degrees
 * \param lng Longitude in degrees
 * \param alt Altitude in feet above sea level
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_add_waypoint(uint16_t n, float lat, float lng, float alt);

/**
 * \brief Pushes a packet to remove a waypoint. Refer sm_remove_waypoint.
 * \param n Waypoint to remove.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_remove_waypoint(uint16_t n);

/**
 * \brief GETs that the vehicle send info about a waypoint. Refer sm_get_waypoint.
 * \param n Waypoint number
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_waypoint(uint16_t n);

/**
 * \brief GETs the number of waypoints from the vehicle. Vehicle will reply with VC_N_WAYPOINTS. Refer vm_n_waypoints.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_n_waypoints();

/**
 * \brief Uploads a geofence point.
 * \param n Geofence point number.
 * \param lat Latitude of geofence point.
 * \param lng Longitude of geofence point.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_add_geofence_point(uint16_t n, float lat, float lng);

/**
 * \brief GETs a geofence point.
 * \param n The geofence point number.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_geofence_point(uint16_t n);

/**
 * \brief Pushes a packet that sets the AHRS installation offset. Refer sm_set_ahrs_installation_offset.
 * The parameters are a quaternion that represent the offset. See push_get_ahrs_quaternion().
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_ahrs_installation_offset(float w, float x, float y, float z);

/**
 * \brief GETs the installation offset of the AHRS. The vehicle will reply with VC_AHRS_OFFSET_REPORT.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_ahrs_offset();

/**
 * \brief GETs the orientation of the AHRS as a quaternion. Vehicle will reply with VC_AHRS_QUATERNION_REPORT. Refer vm_ahrs_quaternion_report.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_ahrs_quaternion();

/**
 * \brief Pushes a packet to GET the vehicle go into magnetometer calibration mode.
 * The vehicle will send a VC_MAG_CAL_STARTING packet and then a VC_MAG_CAL_FINISHED packet on completion.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_calibrate_mag();

/**
 * \brief Pushes a packet to GET the magnetometer calibration. Vehicle will reply with VC_MAG_CAL_REPORT. Refer vm_mag_cal_report.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_mag_cal();

/**
 * \brief Pushes a packet to set the magnetometer calibration.
 * \param magcal An array of 6 short integers that represent min/max for each magnetometer axis.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_mag_cal(fm_mag_cal_pack* magcal);

/**
 * \brief Pushes a GET for servo configuration. The vehicle will reply with a VC_SERVO_CONFIG_REPORT packet. Refer vm_servo_config_report.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_servo_configuration();

/**
 * \brief Pushes a packet to set the servo configuration.
 * \param scale A floating point number that scales
 * \param max_throw An integer that limits the amount of travel that the servos
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_servo_configuration(float scale, int8_t ele_min, int8_t ele_max,
                                                                    int8_t ail_min, int8_t ail_max,
                                                                    int8_t rud_min, int8_t rud_max,
                                                                    int8_t thr_min, int8_t thr_max);

/**
 * \brief GETs pitch PID gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_pitch_gains();

/**
 * \brief Sets the pitch PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_pitch_gains(float kp, float ki, float kd);

/**
 * \brief GETs roll PID gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_roll_gains();

/**
 * \brief Sets the roll PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_roll_gains(float kp, float ki, float kd);

/**
 * \brief GETs heading PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_heading_gains();

/**
 * \brief Sets the heading PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_heading_gains(float kp, float ki, float kd);

/**
 * \brief Gets vert speed PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_vert_speed_gains();

/**
 * \brief Sets the vert speed PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_vert_speed_gains(float kp, float ki, float kd);

/**
 * \brief GETs altitude PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_altitude_gains();

/**
 * \brief Sets the altitude PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_altitude_gains(float kp, float ki, float kd);

/**
 * \brief Use this to set the maximum climb/decent rates.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_max_climb_rates(float max_climb, float max_decend);

/**
 * \brief Use this to get the maximum climb/decent rates.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_max_climb_rates();

/**
 * \brief GETs path PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_path_gains();

/**
 * \brief Sets the path PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_path_gains(float kp, float ki, float kd);

/**
 * \brief GETs autothrottle PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_at_gains();

/**
 * \brief Sets the autothrottle PID controller gains.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_at_gains(float kp, float ki, float kd);

/**
 * \brief GETs the autothrottle settings.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_at_settings();

/**
 * \brief Sets the autothrottle settings.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_at_settings(float pitch_to_throttle, float cruise_speed, int cruise_power, int minimum_throttle);

/**
 * \brief Pushes a GET for mixer configuration.
 * The vehicle will reply with a VC_MIXER_CONFIG_REPORT packet. Refer vm_mixer_config_report.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_mixer_configuration();

/**
 * \brief Pushes a packet to set the mixer configuration.
 * \param config Mixing type.
 * \param mixratio Mixing ratio from 0 to 100. 0 is more elevator.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_mixer_configuration(MIXER_CONFIGURATION config, uint8_t mixratio);

/**
 * \brief Pushes a GET for magnetic variation.
 * The vehicle will reply with a VC_MAG_VAR_REPORT. Refer vm_mag_var_report.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_magnetic_variation();

/**
 * \brief Pushes a packet to set the vehicles magnetic variation.
 * \param mag_var Magnetic variation in degrees.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_magnetic_variation(float mag_var);

/**
 * \brief Pushes a GET for the vehicles loiter settings.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_loiter_settings();

/**
 * \brief Pushes a packet to set the loiter radius and correction strength.
 * \param loiter_radius The size of the loiter radius.
 * \param correction_strength The correction strength.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_loiter_settings(uint32_t loiter_radius, float correction_strength);

/**
 * \brief GETs turn coordination ratios.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_turn_coord_ratios();

/**
 * \brief Pushes a packet to set the yaw coupling ratio.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_turn_coord_ratios(float yc, float btp);

/**
 * \brief GETs maximum angles
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_get_max_angles();

/**
 * \brief Pushes a packet to set the maximum angles.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_set_max_angles(float pitch_down, float pitch_up, float roll);

/**
 * \brief Sets the autothrottle on or off. Depending on the autopilot mode, the autothrottle might be ON regardless.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_autothrottle_pack(bool on);

/**
 * \brief Sets the autolaunch on or off.
 * \return The ID of the packet pushed onto the queue.
 */
        uint16_t push_autolaunch_pack(bool on);

/**
 * \brief GETs the control source. This is the source of control inputs for autopilot.
 */
        uint16_t push_get_control_source();

/**
 * \brief Sets the source of control inputs.
 */
        uint16_t push_set_control_source(CONTROL_SOURCE cs);

/**
 * \brief Sets the battery capacity, so the autopilot can calculate remaining charge.
 */
        uint16_t push_set_battery_mah(uint32_t mah);

/**
 * \brief GETs the battery capacity that the autopilot is used to calculate remaining charge.
 */
        uint16_t push_get_battery_mah();


/**
 * \brief Pushes the vehicle into hardware-in-the-loop test mode.
 * \return The ID of the packet.
 */
        uint16_t push_hil_mode(bool on);

/**
 * \brief Pushes a GET for the number of geofence points.
 * \return The ID of the packet.
 */
        uint16_t push_get_n_geofence_points();

/**
 * \brief Pushes a packet that will remove a geofence point.
 * \return The ID of the packet.
 */
        uint16_t push_remove_geopoint(uint16_t n);

/**
 * \brief Pushes a packet that will remove all geofence points.
 * \return The ID of the packet.
 */
        uint16_t push_clear_geopoints();

/**
 * \brief Sets the automatic actions for the various alarms.
 * \return The ID of the packet.
 */
        uint16_t push_set_alarm_actions(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem);

/**
 * \brief Downloads the alarm actions from the vehicle.
 * \return The ID of the packet.
 */
        uint16_t push_get_alarm_actions();

        uint16_t push_set_max_euler_rates(float pitch, float roll);

        uint16_t push_get_max_euler_rates();

        uint16_t push_set_speed_gain(float sg);

        uint16_t push_get_speed_gain();

        uint16_t push_flight_command(FLIGHT_COMMAND fc);

        uint16_t push_get_accel_cal();

        uint16_t push_set_accel_cal(float x, float y, float z);

/**
 * \brief Sets the selected pitch and bank angle. Units are in hundredths of a degree.
 * \return The ID of the packet.
 */
        uint16_t push_set_selected_pitch_and_roll(float pitch, float roll);

/**
 * \brief Sets the selected heading and altitude. Heading is in hundredths of a degree.
 * Altitude is in feet relative to home altitude.
 * \return The ID of the packet.
 */
        uint16_t push_set_selected_heading_and_alt(float hdg, int32_t alt);

/**
 * \brief Generates the next packet to be sent.
 * \param pack A 20 byte array to store the packet in.
 */
        void next_pack(unsigned char* pack);

/**
 * \brief Decrypts and processes a received packet.
 * \param pack A pointer to the buffer containing the packet.
 * \return -1 on back packet, packet ID on success
 */
        int32_t read_pack(unsigned char* pack);

/**
 * \brief Sets the control data. This data is streamed.
 */
        void set_control_vals(unsigned char v[12]);

/**
 * \brief Returns the heading of the vehicle.
 */
        float get_heading() { return vd.heading/100.0; }
/**
 * \brief Returns the pitch of the vehicle.
 */
        float get_pitch() { return vd.pitch/100.0; }
/**
 * \brief Return the roll of the vehicle.
 */
        float get_roll() { return vd.roll/100.0; }
/**
 * \brief Returns the altitude of the vehicle.
 */
        int get_alt() { return vd.alt; }
/**
 * \brief Returns the vertical speed of the vehicle.
 */
        float get_vert_speed() { return vd.vert_speed/100.0; }
/**
 * \brief Returns the air speed of the vehicle.
 */
        float get_air_speed() { return vd.air_speed/100.0; }

/**
 * \brief Returns the longitude of the vehicle.
 */
        float get_longitude() { return vd.longitude/1000000.0; }
/**
 * \brief Returns the latitude of the vehicle.
 */
        float get_latitude() { return vd.latitude/1000000.0; }
/**
 * \brief Returns the ground speed of the vehicle.
 */
        float get_ground_speed() { return vd.ground_speed/100.0; }
/**
 * \brief Returns the number of satellites used.
 */
        int get_satellites_used() { return vd.satellites_used; }
/**
 * \brief Returns the GPS mode.
 */
        GPS_MODE get_gps_mode() { return vd.gps_mode; }

/**
 * \brief Returns track over ground.
 */
        float get_ground_track() { return vd.ground_track/100.0; }

/**
 * \brief Returns the autopilot mode.
 */
        AUTOPILOT_MODE get_ap_mode() { return vd.ap_mode; }
/**
 * \brief Returns the autopilot pitch bug.
 */
        float get_ap_pitch() { return vd.ap_pitch/100.0; }
/**
 * \brief Returns the autopilot roll bug.
 */
        float get_ap_roll() { return vd.ap_roll/100.0; }
/**
 * \brief Returns the autopilot heading bug.
 */
        float get_ap_heading() { return vd.ap_heading/100.0; }
/**
 * \brief Returns the autopilot altitude bug.
 */
        int32_t get_ap_alt() { return vd.ap_alt; }

/**
 * \brief Returns the latitude of the currently targeted waypoint.
 */
        float get_ap_waypoint_lat() { return vd.wp_lat/1000000.0; }

/**
 * \brief Returns the longitude of the currently targeted waypoint.
 */
        float get_ap_waypoint_lng() { return vd.wp_lng/1000000.0; }

/**
 * \brief Returns the number of the currently targeted waypoint.
 */
        int get_ap_waypoint() { return vd.wp_number; }

/**
 * \brief Returns a number representing the percentage of how much power / fuel is remaining
 */
        int get_power_remaining() { return vd.power_remaining; }
/**
 * \brief Returns true if motor is armed, otherwise false
 */
        bool get_motor_armed() { return vd.motor_armed; }

/**
 * \brief Returns true if the autothrottle is on, otherwise false.
 */
        bool get_autothrottle_on() { return vd.autothrottle_on; }

/**
 * \brief Returns true if the vehicle has signal from a remote control.
 */
        bool get_has_rc_signal() { return vd.has_rc; }

/**
 * \brief Returns true if the vehicle is within a boundary.
 */
        bool get_is_inside_geofence() { return vd.inside_boundary; }

/**
 * \brief Returns the vehicle temperature.
 */
        float get_temperature() { return vd.temperature/100.0; }

/**
 * \brief Returns vehicle timer.
 */
        float get_timer() { return vd.timer/10.0; }

/**
 * \brief Returns vehicle battery voltage.
 */
        float get_volts() { return vd.volts/10.0; }

/**
 * \brief Returns battery milliamps.
 */
        float get_amps() { return vd.milliamps/1000.0; }

/**
 * \brief Returns true is the vehicle is in test mode.
 */
        bool get_hil_mode_on() { return vd.hil_mode; }

/**
 * \brief Returns raw accelerometer and magnetometer readings. Used for calibration.
 */
        vs_raw_acc_mag get_raw_acc_mag_data() { return vd.acc_mag; }

/**
 * \brief Returns a pointer to the debug message buffer.
 */
        char* get_debug_msg_buffer() { return vh_msg; }

/**
 * \brief Returns the number of packets on the inbound queue.
 */
        int in_queue_size();
/**
 * \brief Checks if a packet ID is on the inbound queue.
 * \param id The ID of the packet.
 * \return True if the packet is on the inbound queue.
 */
        bool on_in_queue(uint16_t id);
/**
 * \brief Removes a packet from the inbound queue.
 * \param id The ID of the packet to remove.
 * \return True if the packet was removed.
 */
        bool remove_from_in_queue(uint16_t id);
/**
 * \brief Removes all packets from the inbound queue.
 */
        void clear_in_queue();
/**
 * \brief Retrieves and removes a packet from the queue. The packet data needs to be unpacked using the appropriate struct.
 * \param id The packet ID.
 * \return A fl_pack containing the ID and packet data.
 */
        fl_pack retrieve_from_in_queue(uint16_t id);

/**
 * \brief Use this to enable/disable a streamed packet type.
 * \param id packet ID
 * \param enabled true to enable packet type, false to disable.
 */
        void set_stream_pack_mask(uint16_t id, bool enabled);

        uint16_t push_stream_pack_mask();

    protected:

    private:
        PackQueue in_queue;

        bool add_wp_toggle;
        bool get_wp_toggle;
        bool add_gp_toggle;
        bool get_gp_toggle;

        VehicleData vd;

        uint64_t mask;

        char vh_msg[120]; //debug message from vehicle
};

};

#endif // STATION_H_INCLUDED
