/**
 * \file firelink.h
 *
 * \brief FireLink documentation
 *
 */

/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/**
* \mainpage FireLink Documentation
 *
 * \section intro_sec Introduction
 *
 *
 * FireLink is an extensible communications framework for UAVs and ROVs.<br>
 *
 * FireLink is being developed for the FireTail UAV project. <br>
 *
 * This API can be used to develop either a Vehicle (UAV, ROV, whatever) or a Station.
 *
 * FireLink Vehicles and Stations can use serial radio modems, internet/network connection (UDP)
 * or anything else. It doesn't really matter how you send the data; if you can send binary data between
 * your station and vehicle, you can use the FireLink API. This is because the
 * FireLink API only creates and interprets packets. Sending and receiving will depend on your code. <br>
 *
 * FireLink vehicles will automatically transfer information such as heading,
 * pitch, roll, speed and so on. FireLink stations will transfer control
 * information to directly drive up to 12 servo motors or perform other
 * functions. These types of packets are referred to as streaming packets. <br>
 *
 * Both vehicles and stations can transmit messages. Messages are gauranteed to be
 * received at the other end without blocking streaming data. There is a complete suite of built
 * in messages which should cover the requirements for a minimal UAV, however additional messages can be easily added. <br>
 *
 * FireLink features 256bit AES encryption. The purpose of encryption is to
 * to prevent the possibility of eaves droppers being able to interpret real time
 * streaming data and hijacking the vehicle. It is not designed to be a complete
 * security solution. It will not prevent jamming
 * or protect against injected data. <br>
 *
 * \section download Downoad the API
 * <a href="http://www.camelsoftware.com/firelink/download">Download here</a><br>
 * Make sure the version number of the station and vehicle match. <br>
 *
 * \section packet_structure Packet Structure
 * FireLink packets contain only 20 bytes of data. The reason for the tiny packet size is that small packets of data are less likely to be corrupted and discarded than large packets.
 * This makes FireLink more useable for lossy transmission methods (like serial radios). <br><br>
 * In each packet there is an ID, data section, ACK code, checksum and packet tail. <br><br>
 * The ID is a an unsigned two byte integer. It determines the type of data being transfered. Vehicles and Stations have different sets of packets, so a Vehicle packet of ID 1 will be different to a Station packet of ID 1.
 * IDs from 0 - 127 are reserved for streaming data packets.<br><br>
 * ID from 128 to 1023 are reserved for built in message types. The remaining IDs 1024 to 65535 are available for custom packet types. <br><br>
 * The data section measures 12 bytes. This could also be called the payload. <br><br>
 * The ACK (acknowledge) code is an unsigned two byte integer. The ACK code is the ID of the last received message packet and it's a way of notifying the sender that the message has been received.
 * FireLink handles the ACK bytes internally. <br><br>
 * If encryption is enabled, the ID, data section and ACK bytes are encrypted.<br>
 * The checksum is a CRC-16 checksum that measures two bytes. This checksum algorithm is able to detect 99.9984% of errors. <br>
 * The packet tail is simply a newline character followed by a null terminator. The C escape sequences for these are '\\n\0'. Good packets will always have a '\\n\0' packet tail and a valid checksum.
 * Invalid packets are ignored and discarded.
 *
 * \section transfer_types Data Transfer
 * FireLink transfers data in two different way. <br><br>
 * Streamed data is transfered transparently and 'automatically'.
 * It is called streamed data because it is similar to streaming music or video. Lost data is
 * never detected and streamed data packets are not acknowledged by the receiver. Streamed packets are sent continuously
 * to ensure the receiver always has an up-to-date copy of the data. <br><br>
 * Message packets on the other hand are queued up until the opportunity to send them arises. The queue is refered to the dispatch queue,
 * and to send a message it must be pushed to the top of the dispatch queue. There is a number of built in messages available that should
 * cover most peoples needs. <br><br>
 * When there are messages on the dispatch queue, FireLink will switch between sending the first message on the queue and sending
 * streamed data packets. When the receiver acknowledges the message, it is removed from the dispatch queue.
 *
 * \section protocol Protocol
 * The protocol is a simple point to point protocol. The Station transmits and the Vehicle replies. The vehicle should continue transmitting for some time after receiving a valid packet.
 * The length of time is determined by Station keep-alive packets.
 * If the station has no need to transmit but needs to continue receiving data, it can send a keep-alive packet to force the vehicle to continue transmitting.
 * See the Keep-Alive section below.
 * This is to ensure neither the Station or Vehicle are transmitting needlessly, which is a waste of power and a potential security risk. <br><br>
 * Some Station packet types require the Vehicle to send a specific reply. For example, a SC_GET_SERVO_CONFIG packet (generated by Station::push_GET_servo_configuration())
 * requires the Vehicle to reply with a VC_SERVO_CONFIGURATION_REPORT packet (generated by Vehicle::push_servo_configuration_report()). Refer to the documentation for the specific functions for more details. <br>
 *
 * \section repeat_suppression Repeat Packet Suppression
 * FireLink will send messages continously until they are acknowledged by the receiver. It is possible that a message will be
 * transmitted OK but the acknowledgement is not received. In this case the sender will continue sending and the same message could be received multiple times.
 * FireLink will ignore received messages if they have the same ID and data bytes as the last received message. Keep-alive packets are an exception, they can be sent multiple times.<br><br>
 * If you have a legitimate need to send identical messages multiple times, you could push a custom packet with an unused ID to break the repeat packet suppression.
 *
 * \section vehicle_design Vehicle Design
 * FireLink is designed for C++ and uses object-oriented techniques. The Vehicle class should be inherited by your own class.<br>
 *
 * Your vehicle class should be responsible for the control loop. The communication section of the control loop should run at
 * 50Hz. <br>
 *
 * Here is a code example of a FireLink based vehicle communication loop.<br>
 * @code

    bool rps = false;                               //flag to mark if a packet was received
    unsigned char pack[40];                         //buffer to store received data
    if(Serial2.available() >= 20)                   //packets are 20 byte long
                                                    //so don't bother unless there are more than 20 bytes available
    {
        int i = 0;
        while(Serial2.available())
        {
            pack[i] = Serial2.read();               //read a byte from the serial port
            if(i >= 20)                             //if 20 or more bytes are received
            {
                rps = read_pack(&pack[i-20]);       //try to read the packet
                if(rps)                             //if packet was sent ok, send a reply
                {
                    next_pack(pack);                //generate the next packet
                    Serial2.write(pack, 20);        //write the packet
                    break;                          //and break the loop
                }
            }
            i++;
            if(i > 39)                              //avoid exceeding array length
                break;
        }
    }
 * @endcode
 *
 * The streamed vehicle data must be kept up to date to ensure that the station always has a valid copy of the data.<br><br>
 * Refer to the VehicleData class to see what vehicle data is streamed.<br><br>
 * Data that is usually represented by floating point numbers is converted to integer types. Latitude and longitude for example
 * are multiplied by a million and sent as integers that represent latitude and longitude as millionths of a degree. <br><br>
 * Here is a code example
 * @code
imu::Vector euler = AHRS::get_euler();

vd.heading = euler.x()*100;                 //heading, pitch, roll as hundredths of a degree
vd.pitch = euler.y()*100;
vd.roll = euler.z()*100;
vd.alt = AHRS::get_alt();                   //barometic alt


vd.latitude = gps.latitude * 1000000;       //lat and lng as millionths of a degree
vd.longitude = gps.longitude * 1000000;
vd.ground_speed = gps.speed;
vd.satellites_used = gps.sats_in_use;
vd.gps_mode = gps.fix;
 * @endcode
 * The vehicle must be able to receive and respond to messages from the Station.
 * To do this, the vehicle class must override the message handling functions. These are virtual functions that all start with on_.
 * The message handlers are called by the read_pack() function whenever a valid message is received.
 * Overriden message handlers must return true if they actually handle the packet.<br>
 * Here is an example of the arm motor packet being received.
 * @code
bool Firetail::on_arm_motor_recv(bool arm)
{
    vd.motor_armed = arm;
    return true;
}
 * @endcode
 * Sometimes the vehicle must respond to a message by sending a reply. This can be done by using the relevent push_ function.
 * Here is an example of an on_GET_ahrs_quaternion handler.
 * @code
bool Firetail::on_GET_ahrs_quaternion()
{
    imu::Quaternion rot = AHRS::get_ahrs_quaternion();
    push_ahrs_quaternion_report(rot.w(), rot.x(), rot.y(), rot.z());
    return true;
}
 * @endcode
 * \section station_design Station Design
 * The Station class may also be inherited, although this isn't strictly neccessary. An IS_A relationship is neccessary to override the
 * message handlers, but Stations also queue inbound packets. Storing packets in an queue is much more convenient for
 * multithreaded applications where a section of code outside the communications thread needs to receive the message. <br><br>
 * Stations need to have a communications loop. Typically this will run in a separate thread to avoid interfering with the GUI. <br>
 * Here is a basic code example of a station communications loop.
 * @code
unsigned char buf[60];

if((!get_disable_stream()) || (queue_size() > 0))   //if streamed data is NOT disabled
                                                    //OR there is a message on the dispatch queue
{
    next_pack(buf);                                 //generate the next packet
    sp.write(buf, 20);                              //and send it off
}

if(sp.available() < 20)                             //if there is less than 20 bytes waiting to be received
    return false;                                   //don't bother reading

memset(buf, 60, '\0');

int i = 0;
while(sp.available() && (i < 60))                   //while there is data available and the maximum length hasn't been exceeded
{
    buf[i] = sp.read(1)[0];                         //read a byte
    if(buf[i] == '\n')                              //if it's a newline character it might be the start of a packet ending
    {
        if(read_pack(&buf[i-18]))                   //so try to read the packet
        {
            return true;                            //and if it went well, return true
        }
    }
    i++;
}
 * @endcode
 * Have a look at the Coms class in FLStation to see an example of a full featured communications loop.<br><br>
 * Stations can push and receive messages just like a Vehicle but this is often inconvenient for multithreaded applications so
 * Stations also handle message packets by queuing them up on an inbound packet queue.
 * Only messages that are not handled by an overriden message handler are placed on the inbound queue. <br>
 * Unlike messages received by the handler functions, packets that are on the inbound queue are not processed in any way.
 * They contain raw packet data only and must be unpacked using the appropriate packet structure. <br>
 * To check if a packet is on the inbound queue, the on_in_queue() function can be used.<br>
 * To retrieve a packet off the inbound queue, the retrieve_from_in_queue() function can be used.<br>
 * Here is an example of a Station GETing an AHRS offset quaternion.<br>
 * @code
coms->lock();                                                           //lock the mutex for multithreading safety
coms->push_GET_ahrs_quaternion();                                   //push a GET for the ahrs quaternion
coms->unlock();                                                         //unlock the mutex

int tries = 0;
while(tries < 100)                                                      //wait for up to two seconds for the packet to arrive
{
    std::this_thread::sleep_for(std::chrono::milliseconds(20));         //pause for 20ms
    coms->lock();                                                       //lock the mutex
    bool oq = coms->on_in_queue(FireLink::VC_AHRS_QUATERNION_REPORT);   //check if the quaternion report has arrived yet
    coms->unlock();                                                     //unlock immediately

    if(oq)                                                              //if the packet is on the queue
        break;                                                          //break the loop
    tries++;
}

if(tries >= 100)                                                        //if the packet didn't arrive, show an error message and return
{
    Gtk::MessageDialog msg(*this, "No reponse!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The vehicle did not reply to the GET for an AHRS reading.");
    msg.run();
    return;
}
tries = 0;

coms->lock();                                                           //lock the mutex
FireLink::fl_pack qr = coms->retrieve_from_in_queue(FireLink::VC_AHRS_QUATERNION_REPORT); //retrieve the packet from the queue
                                                                        //this function removes the packet from the queue too

FireLink::vm_ahrs_quaternion_report aqr;                                //use the correct struct to unpack the data
memcpy(&aqr, qr.data, sizeof(aqr));                                     //unpack the data

float qw, qx, qy, qz;                                                   //floating point types are usually compressed into integer types
qw = aqr.w/10000.0;                                                     //so they must be converted back to floats
qx = aqr.x/10000.0;
qy = aqr.y/10000.0;
qz = aqr.z/10000.0;
 * @endcode
 * \section encryption Encryption
 * FireLink has 256bit AES encryption built in. To use it, set the key and initialisation vector ( Link::set_key() and Link::set_iv() ) and then
 * set Link::encryption_on to true.
 * \section stream_disable Disabling Streamed Data
 * Sometimes it is useful to disable streamed data. Stations that don't need to transmit control data are a good example. Streamed data can be
 * turned off using Link::set_disable_stream(). <br>
 * Disabling streamed data also increases the rate of transfer of messages, which can be useful when transfering large amounts of data.
 * \section keepalive Keep-Alive
 * When a Station isn't streaming control data, it probably won't be transmitting most of the time. To keep the datalink alive, Stations should send
 * keep-alive packets periodically. Keep-alive packets tell the vehicle to continue transmitting for a specified number of seconds after it has received a valid packet.
 * FLStation transmits 5 second keep-alive packets every 1 second.  <br>
 * Keep-alive packets can be pushed onto the station dispatch queue at a low rate (once every second or more).
 * They force the vehicle to transmit for a specified number of seconds after receiving a valid packet. <br>
 * Here is a code example of how a vehicle should handle keep-alive packets.
 * @code

unsigned long last_read_millis;

bool Firetail::on_keepalive_recv(uint32_t seconds)          //override the handler
{
    keepalive_millis = millis();                            //save the time that the packet arrived
    keepalive_seconds = seconds;                            //save the keepalive duration
    return true;
}

...
//STANDARD COMMUNICATION LOOP
unsigned char pack[40];
if(Serial2.available() >= 30)
{
    int i = 0;
    while(Serial2.available())
    {
        pack[i] = Serial2.read();
        if(i >= 20)
        {
            if(read_pack(&pack[i-20]))
                last_read_millis = millis();
        }
        i++;
        if(i > 39) //avoid exceeding array length
            break;
    }
}

if(((millis()-last_read_millis) > (keepalive_seconds*1000)))
{
    next_pack(pack);                                    //get the next packet
    Serial2.write(pack, 20);                            //and send it off
}

 * @endcode
 * \section embedded Embedded Systems & Microcontrollers
 * FireLink will run on any device that has sufficient memory to carry it. One of the most resource consuming parts of FireLink is the encryption library.
 * Encryption can be removed by defining NO_ENCRYPTION in link.h. This will save at least 1KB of memory. <br>
 * FireLink has been tested on the Arduino Mega 2560 (with no encryption) and the Arduino Due.
 * \section more_help Further Assistance
 * For more assistance, <a href="http://www.camelsoftware.com/firetail/index.php/contact">contact me</a>
*/
#ifndef FIRELINK_H_INCLUDED
#define FIRELINK_H_INCLUDED

#include "station.h"
#include "vehicle.h"


#endif // FIRELINK_H_INCLUDED
