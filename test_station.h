#ifndef TS_H
#define TS_H

#include "firelink.h"

using namespace FireLink;

class mystation : public FireLink::Station
{
    public:
        mystation()
        {
            repeat_pack = 0;
        }

        bool on_geofence_point_reported(uint16_t n, float lat, float lng);
        bool on_n_geofence_points_reported(uint16_t n);

        bool on_alarm_actions_reported(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem);

        bool on_alarm_activated(ALARM_TYPE alarm);

        bool on_custom_recv(uint16_t pid, unsigned char* data);

        bool on_max_euler_rates_reported(float pitch, float roll);
        bool on_speed_gain_reported(float sg);

        bool on_servo_configuration_reported(float s, int8_t a, int8_t a1, int8_t a2, int8_t a3, int8_t a4, int8_t a5, int8_t a6, int8_t a7);

        bool on_vert_speed_gains_reported(float kp, float ki, float kd);

        bool on_debug_msg_received(char* msg);


        uint16_t last_gp_n;
        float last_gp_lat, last_gp_lng;

        float vs_kp, vs_ki, vs_kd;

        int n_gp_points;

        ALARM_ACTION low_bat_action;
        ALARM_ACTION boundary_action;
        ALARM_ACTION no_gps_action;
        ALARM_ACTION lost_control_action;
        ALARM_ACTION no_telem_action;

        ALARM_TYPE last_alarm;

        int repeat_pack;

        float max_pitch, max_roll;
        float speed_gain;
};

#endif // TS_H
