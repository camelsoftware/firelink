/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include "station.h"

namespace FireLink
{

Station::Station()
{
    set_vd(&vd);

    skip_queue = false;
    encryption_on = false;
    in_queue.pNext = 0;
}

Station::~Station()
{
    clear_in_queue();
}

void Station::set_control_vals(unsigned char* vals)
{
    memcpy(control_vals, vals, 12);
}

void Station::set_stream_pack_mask(uint16_t id, bool enabled)
{
    mask = (mask & ~(1<<id)) | (enabled << id);
}

uint16_t Station::push_stream_pack_mask()
{
    fl_pack pack;
    pack.id = SC_SET_STREAM_MASK;
    uint64_t* msk = (uint64_t*)pack.data;
    *msk = mask;
    push_custom_pack(pack);
    return SC_SET_STREAM_MASK;
}

uint16_t Station::push_keep_alive_pack(uint32_t seconds)
{
    fl_pack pack;
    pack.id = SC_KEEP_ALIVE;
    sm_keep_alive df;
    df.seconds = seconds;
    memcpy(pack.data, &df, sizeof(df));
    return push_custom_pack(pack);
}

uint16_t Station::push_arm_motor_pack(bool arm)
{
    fl_pack pack;
    pack.id = SC_ARM_MOTOR;
    sm_arm_motor am;
    am.arm = arm;
    memcpy(pack.data, &am, sizeof(am));
    return push_custom_pack(pack);
}

uint16_t Station::push_set_ap_mode_pack(AUTOPILOT_MODE mode)
{
    fl_pack pack;
    pack.id = SC_SET_AP_MODE;
    sm_set_ap_mode sap;
    sap.mode = mode;
    memcpy(pack.data, &sap, sizeof(sap));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_autopilot_status()
{
    fl_pack pack;
    pack.id = SC_GET_AUTOPILOT_STATUS;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_satellite_report(unsigned char sat_number)
{
    fl_pack pack;
    pack.id = SC_GET_SATELLITE_REPORT;
    sm_get_satellite_report rsr;
    rsr.sat_n = sat_number;
    memcpy(pack.data, &rsr, sizeof(rsr));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_home_coordinates()
{
    fl_pack pack;
    pack.id = SC_GET_HOME_COORDINATES;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_home_coordinates(float lat, float lon, float alt)
{
    fl_pack pack;
    pack.id = SC_SET_HOME_COORDINATES;
    fm_waypoint_pack* wp = (fm_waypoint_pack*)pack.data;
    wp->lat = roundf(lat*1000000.0);
    wp->lng = roundf(lon*1000000.0);
    wp->alt = alt+1000;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_alt_source(ALTIMETER_SOURCE as)
{
    fl_pack pack;
    pack.id = SC_SET_ALTIMETER_SOURCE;
    sm_set_altimeter_source sas;
    sas.alt_source = as;
    memcpy(pack.data, &sas, sizeof(sas));
    return push_custom_pack(pack);
}

uint16_t Station::push_clear_waypoints()
{
    fl_pack pack;
    pack.id = SC_CLEAR_WAYPOINTS;
    return push_custom_pack(pack);
}

uint16_t Station::push_add_waypoint(uint16_t n, float lat, float lng, float alt)
{
    fl_pack pack;
    if(add_wp_toggle)
    {
        pack.id = SC_ADD_WAYPOINT;
        add_wp_toggle = false;
    }
    else
    {
        pack.id = SC_ADD_WAYPOINT2;
        add_wp_toggle = true;
    }

    fm_waypoint_pack* aw = (fm_waypoint_pack*)pack.data;
    aw->lat = roundf(lat*1000000.0);
    aw->lng = roundf(lng*1000000.0);
    aw->alt = roundf(alt) + 1000;
    aw->n = n;
    return push_custom_pack(pack);
}

uint16_t Station::push_remove_waypoint(uint16_t n)
{
    fl_pack pack;
    pack.id = SC_REMOVE_WAYPOINT;
    sm_remove_waypoint rw;
    rw.waypoint = n;
    memcpy(pack.data, &rw, sizeof(rw));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_waypoint(uint16_t n)
{
    fl_pack pack;
    if(get_wp_toggle)
    {
        pack.id = SC_GET_WAYPOINT;
        get_wp_toggle = false;
    }
    else
    {
        pack.id = SC_GET_WAYPOINT2;
        get_wp_toggle = true;
    }

    sm_get_waypoint gw;
    gw.waypoint = n;
    memcpy(pack.data, &gw, sizeof(gw));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_n_waypoints()
{
    fl_pack pack;
    pack.id = SC_GET_N_WAYPOINTS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_ahrs_installation_offset(float w, float x, float y, float z)
{
    fl_pack pack;
    pack.id = SC_SET_AHRS_INSTALLATION_OFFSET;
    sm_set_ahrs_installation_offset saio;
    saio.w = roundf(w * 10000.0);
    saio.x = roundf(x * 10000.0);
    saio.y = roundf(y * 10000.0);
    saio.z = roundf(z * 10000.0);
    memcpy(pack.data, &saio, sizeof(saio));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_ahrs_offset()
{
    fl_pack pack;
    pack.id = SC_GET_AHRS_INSTALLATION_OFFSET;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_ahrs_quaternion()
{
    fl_pack pack;
    pack.id = SC_GET_AHRS_QUATERNION;
    return push_custom_pack(pack);
}

uint16_t Station::push_calibrate_mag()
{
    fl_pack pack;
    pack.id = SC_CALIBRATE_MAGNETOMETERS;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_mag_cal()
{
    fl_pack pack;
    pack.id = SC_GET_MAGNETOMETER_CAL;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_mag_cal(fm_mag_cal_pack* magcal)
{
    fl_pack pack;
    pack.id = SC_SET_MAGNETOMETER_CAL;
    memcpy(pack.data, magcal, sizeof(fm_mag_cal_pack));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_accel_cal()
{
    fl_pack pack;
    pack.id = SC_GET_ACCELEROMETER_CAL;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_accel_cal(float x, float y, float z)
{
    fl_pack pack;
    pack.id = SC_SET_ACCELEROMETER_CAL;
    fm_accel_cal_pack* ac = (fm_accel_cal_pack*)pack.data;
    ac->x_bias = roundf(x*10000);
    ac->y_bias = roundf(y*10000);
    ac->z_bias = roundf(z*10000);
    return push_custom_pack(pack);
}

uint16_t Station::push_get_servo_configuration()
{
    fl_pack pack;
    pack.id = SC_GET_SERVO_CONFIG;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_servo_configuration(float scale, int8_t ele_min, int8_t ele_max,
                                                                    int8_t ail_min, int8_t ail_max,
                                                                    int8_t rud_min, int8_t rud_max,
                                                                    int8_t thr_min, int8_t thr_max)
{
    fl_pack pack;
    pack.id = SC_SET_SERVO_CONFIG;

    fm_servo_config ssc;
    int16_t iscale = roundf(scale * 100.0);
    ssc.scale = iscale;
    ssc.ele_min = ele_min;
    ssc.ele_max = ele_max;
    ssc.ail_min = ail_min;
    ssc.ail_max = ail_max;
    ssc.rud_min = rud_min;
    ssc.rud_max = rud_max;
    ssc.thr_min = thr_min;
    ssc.thr_max = thr_max;

    memcpy(pack.data, &ssc, sizeof(ssc));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_mixer_configuration()
{
    fl_pack pack;
    pack.id = SC_GET_MIXER_CONFIG;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_mixer_configuration(MIXER_CONFIGURATION config, uint8_t mixratio)
{
    fl_pack pack;
    pack.id = SC_SET_MIXER_CONFIG;
    sm_set_mixer_config smc;
    smc.config = config;
    smc.mixratio = mixratio;
    memcpy(pack.data, &smc, sizeof(smc));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_pitch_gains()
{
    fl_pack pack;
    pack.id = SC_GET_PITCH_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_pitch_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_PITCH_GAINS;
    fm_pid_pack pp;
    pp.kp = kp * 10000;
    pp.ki = ki * 10000;
    pp.kd = kd * 10000;
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_roll_gains()
{
    fl_pack pack;
    pack.id = SC_GET_ROLL_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_roll_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_ROLL_GAINS;
    fm_pid_pack pp;
    pp.kp = kp * 10000;
    pp.ki = ki * 10000;
    pp.kd = kd * 10000;
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_heading_gains()
{
    fl_pack pack;
    pack.id = SC_GET_HEADING_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_heading_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_HEADING_GAINS;
    fm_pid_pack pp;
    pp.kp = kp * 10000;
    pp.ki = ki * 10000;
    pp.kd = kd * 10000;
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_vert_speed_gains()
{
    fl_pack pack;
    pack.id = SC_GET_VERT_SPEED_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_vert_speed_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_VERT_SPEED_GAINS;
    fm_pid_pack pp;
    pp.kp = kp * 10000;
    pp.ki = ki * 10000;
    pp.kd = kd * 10000;
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}


uint16_t Station::push_get_altitude_gains()
{
    fl_pack pack;
    pack.id = SC_GET_ALT_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_altitude_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_ALT_GAINS;
    fm_pid_pack pp;
    pp.kp = roundf(kp * 10000);
    pp.ki = roundf(ki * 10000);
    pp.kd = roundf(kd * 10000);
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}

uint16_t Station::push_set_max_climb_rates(float max_climb, float max_decent)
{
    fl_pack pack;
    pack.id = SC_SET_MAX_CLIMB;
    fm_max_climb_pack* mc = (fm_max_climb_pack*)pack.data;
    mc->climb = roundf(max_climb*10);
    mc->decend = roundf(max_decent*10);
    return push_custom_pack(pack);
}

uint16_t Station::push_get_max_climb_rates()
{
    fl_pack pack;
    pack.id = SC_GET_MAX_CLIMB;
    return push_custom_pack(pack);
}


uint16_t Station::push_get_path_gains()
{
    fl_pack pack;
    pack.id = SC_GET_PATH_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_path_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_PATH_GAINS;
    fm_pid_pack pp;
    pp.kp = kp * 10000;
    pp.ki = ki * 10000;
    pp.kd = kd * 10000;
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_at_gains()
{
    fl_pack pack;
    pack.id = SC_GET_AT_GAINS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_at_gains(float kp, float ki, float kd)
{
    fl_pack pack;
    pack.id = SC_SET_AT_GAINS;
    fm_pid_pack pp;
    pp.kp = kp * 10000;
    pp.ki = ki * 10000;
    pp.kd = kd * 10000;
    memcpy(pack.data, &pp, sizeof(pp));
    return push_custom_pack(pack);
}


uint16_t Station::push_get_magnetic_variation()
{
    fl_pack pack;
    pack.id = SC_GET_MAG_VAR;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_magnetic_variation(float mag_var)
{
    fl_pack pack;
    pack.id = SC_SET_MAG_VAR;
    sm_set_mag_var smv;
    smv.mag_var = roundf(mag_var*100.0);
    memcpy(pack.data, &smv, sizeof(smv));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_loiter_settings()
{
    fl_pack pack;
    pack.id = SC_GET_LOITER_SETTINGS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_loiter_settings(uint32_t loiter_radius, float correction_strength)
{
    fl_pack pack;
    pack.id = SC_SET_LOITER_SETTINGS;

    loiter_settings_pack lsp;
    lsp.loiter_radius = loiter_radius;
    lsp.correction_strength = correction_strength * 100;
    memcpy(pack.data, &lsp, sizeof(lsp));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_turn_coord_ratios()
{
    fl_pack pack;
    pack.id = SC_GET_TURN_COORD_RATIOS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_turn_coord_ratios(float yc, float ptb)
{
    fl_pack pack;
    pack.id = SC_SET_TURN_COORD_RATIOS;

    fm_turn_coord_pack* ycp = (fm_turn_coord_pack*)pack.data;
    ycp->yc_ratio = roundf(yc*100);
    ycp->bank_pitch_ratio = roundf(ptb*1000);
    return push_custom_pack(pack);
}

uint16_t Station::push_get_max_angles()
{
    fl_pack pack;
    pack.id = SC_GET_MAX_ANGLES;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_max_angles(float pitch_down, float pitch_up, float roll)
{
    fl_pack pack;
    pack.id = SC_SET_MAX_ANGLES;
    fm_max_angles* ma = (fm_max_angles*)pack.data;
    ma->pitch_down = roundf(pitch_down*100);
    ma->pitch_up = roundf(pitch_up*100);
    ma->roll = roundf(roll*100);
    return push_custom_pack(pack);
}


uint16_t Station::push_hil_mode(bool on)
{
    fl_pack pack;
    pack.id = SC_HIL_MODE;

    sm_hil_mode sh;
    sh.hm = on;
    memcpy(pack.data, &sh, sizeof(sh));
    return push_custom_pack(pack);
}

uint16_t Station::push_get_at_settings()
{
    fl_pack pack;
    pack.id = SC_GET_AT_SETTINGS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_at_settings(float pt, float cs, int cp, int minthr)
{
    fl_pack pack;
    pack.id = SC_SET_AT_SETTINGS;
    fm_at_settings* as = (fm_at_settings*)pack.data;
    as->pt = roundf(pt*100);
    as->cs = roundf(cs*100);
    as->cp = cp;
    as->min_thr = minthr;
    return push_custom_pack(pack);
}

uint16_t Station::push_autothrottle_pack(bool on)
{
    fl_pack pack;
    pack.id = SC_AUTOTHROTTLE;
    sm_autothrottle* sa = (sm_autothrottle*)pack.data;
    sa->on = on;
    return push_custom_pack(pack);
}

uint16_t Station::push_autolaunch_pack(bool on)
{
    fl_pack pack;
    pack.id = SC_AUTOLAUNCH;
    sm_autothrottle* sa = (sm_autothrottle*)pack.data;
    sa->on = on;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_control_source()
{
    fl_pack pack;
    pack.id = SC_GET_CONTROL_SOURCE;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_control_source(CONTROL_SOURCE cs)
{
    fl_pack pack;
    pack.id = SC_SET_CONTROL_SOURCE;
    fm_control_source* csp = (fm_control_source*)pack.data;
    csp->control_source = cs;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_battery_mah(uint32_t mah)
{
    fl_pack pack;
    pack.id = SC_SET_BATTERY_MAH;
    fm_battery_mah* ba = (fm_battery_mah*)pack.data;
    ba->mah = mah;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_battery_mah()
{
    fl_pack pack;
    pack.id = SC_GET_BATTERY_MAH;
    return push_custom_pack(pack);
}

uint16_t Station::push_add_geofence_point(uint16_t n, float lat, float lng)
{
    fl_pack pack;
    if(add_gp_toggle)
    {
        pack.id = SC_ADD_GEOPOINT;
        add_gp_toggle = false;
    }
    else
    {
        pack.id = SC_ADD_GEOPOINT2;
        add_gp_toggle = true;
    }

    fm_waypoint_pack* wp = (fm_waypoint_pack*)pack.data;
    wp->n = n;
    wp->lat = roundf(lat*1000000);
    wp->lng = roundf(lng*1000000);
    return push_custom_pack(pack);
}

uint16_t Station::push_get_geofence_point(uint16_t n)
{
    fl_pack pack;
    if(get_gp_toggle)
    {
        pack.id = SC_GET_GEOPOINT;
        get_gp_toggle = false;
    }
    else
    {
        pack.id = SC_GET_GEOPOINT2;
        get_gp_toggle = true;
    }

    sm_get_waypoint* wp = (sm_get_waypoint*)pack.data;
    wp->waypoint = n;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_n_geofence_points()
{
    fl_pack pack;
    pack.id = SC_GET_N_GEOPOINTS;
    return push_custom_pack(pack);
}

uint16_t Station::push_remove_geopoint(uint16_t n)
{
    fl_pack pack;
    pack.id = SC_REMOVE_GEOPOINT;
    sm_remove_waypoint* rm = (sm_remove_waypoint*)pack.data;
    rm->waypoint = n;
    return push_custom_pack(pack);
}

uint16_t Station::push_clear_geopoints()
{
    fl_pack pack;
    pack.id = SC_CLEAR_GEOPOINTS;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_max_euler_rates(float pitch, float roll)
{
    fl_pack pack;
    pack.id = SC_SET_MAX_EULER_RATES;
    fm_max_euler_rates* mr = (fm_max_euler_rates*)pack.data;
    mr->pitch = roundf(pitch*100.0);
    mr->roll = roundf(roll*100.0);
    return push_custom_pack(pack);
}

uint16_t Station::push_get_max_euler_rates()
{
    fl_pack pack;
    pack.id = SC_GET_MAX_EULER_RATES;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_speed_gain(float sg)
{
    fl_pack pack;
    pack.id = SC_SET_SPEED_GAIN;
    int32_t* isg = (int32_t*)pack.data;
    (*isg) = roundf(sg*10000.0);
    return push_custom_pack(pack);
}

uint16_t Station::push_get_speed_gain()
{
    fl_pack pack;
    pack.id = SC_GET_SPEED_GAIN;
    return push_custom_pack(pack);
}


uint16_t Station::push_set_alarm_actions(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem)
{
    fl_pack pack;
    pack.id = SC_SET_ALARM_ACTIONS;
    fm_alarm_actions* faa = (fm_alarm_actions*)pack.data;
    faa->low_bat = lb;
    faa->boundary = ba;
    faa->no_gps_lock = gpsa;
    faa->lost_control = lca;
    faa->no_telem = no_telem;
    return push_custom_pack(pack);
}

uint16_t Station::push_get_alarm_actions()
{
    fl_pack pack;
    pack.id = SC_GET_ALARM_ACTIONS;
    return push_custom_pack(pack);
}

uint16_t Station::push_flight_command(FLIGHT_COMMAND fc)
{
    fl_pack pack;
    pack.id = SC_FLIGHT_COMMAND;
    FLIGHT_COMMAND* cmd = (FLIGHT_COMMAND*)pack.data;
    *cmd = fc;
    return push_custom_pack(pack);
}

uint16_t Station::push_set_selected_pitch_and_roll(float pitch, float roll)
{
    fl_pack pack;
    pack.id = SC_SET_PNR;
    fm_select_pnr_pack* sa = (fm_select_pnr_pack*)pack.data;
    sa->pitch = roundf(pitch*100);
    sa->roll = roundf(roll*100);
    return push_custom_pack(pack);
}

uint16_t Station::push_set_selected_heading_and_alt(float heading, int32_t alt)
{
    fl_pack pack;
    pack.id = SC_SET_HNA;
    fm_select_hna_pack* sa = (fm_select_hna_pack*)pack.data;
    sa->heading = roundf(heading*100);
    sa->alt = alt;
    return push_custom_pack(pack);
}


void Station::next_pack(unsigned char* out)
{
    unsigned char buf[20];
    fl_pack pack;


    if((queue_size() > 0) && ((!skip_queue)||(no_stream)))//if there is a pack queued
    {
        pack = next_off_queue();
        memcpy(buf, &pack, 14);
        skip_queue = true;
    }
    else
    {
        skip_queue = false;

        pack.id = SS_CONTROL;
        memcpy(pack.data, control_vals, 12);
        memcpy(buf, &pack, 14);
    }

    memcpy(buf+14, &ack, 2);

#ifndef NO_ENCRYPTION
    if(encryption_on)
    {
        unsigned char IV[16];
        memcpy(IV, init_vec, 16);
        aes.cbc_encrypt(buf, out, 1, IV);
    }
    else
    {
        memcpy(out, buf, 16);
    }
#else
    memcpy(out, buf, 16);
#endif

    uint16_t cs = make_checksum((unsigned char*)out);
    memcpy(out+16, &cs, 2);
    out[18] = '\n';
    out[19] = '\0';
}

int32_t Station::read_pack(unsigned char* pack)
{
    unsigned char decrypt_buf[16];
    unsigned char* data;
    uint16_t id;

    if(!check_checksum(pack))
        return -1;

#ifndef NO_ENCRYPTION
    if(encryption_on)
    {
        //decrypt
        unsigned char IV[16];
        memcpy(IV, init_vec, 16);
        aes.cbc_decrypt(pack, decrypt_buf, 1, IV);
    }
    else
    {
        memcpy(decrypt_buf, pack, 16);
    }
#else
    memcpy(decrypt_buf, pack, 16);
#endif

    id = *(uint16_t*)decrypt_buf;
    data = decrypt_buf + 2;
    ack = id; //save the ack

    //remove commands from queue if they are ACKed
    uint16_t inack = *(uint16_t*)(decrypt_buf+14);
    if(next_off_queue().id == inack) //if it's on the queue, remove it
        remove_pack(inack);


    //repeated packet supression - transmitter will continue sending until it gets a valid ACK
    //so the receiver will probably get the same packet multiple times.
    if((id == last_msg_id))
    {
        if(memcmp(last_msg_data, data, 12) == 0)
            return id;
    }
    if(id >= 128)
    {
        last_msg_id = id;
        memcpy(last_msg_data, data, 12);
    }

    bool ret = false;
    switch(id)
    {
    case VS_NULL:
    {
        ret = true;
    }
    break;
    case VS_INST_1: //instrument data type 1
    {
        vs_inst_1 vi;

        memcpy(&vi, data, sizeof(vi));

        vd.heading = vi.heading;
        vd.pitch = vi.pitch;
        vd.roll = vi.roll;
        vd.alt = vi.baroalt - 1000;
        vd.vert_speed = vi.vert_speed;
        vd.air_speed = vi.air_speed;

        ret = true;
    }
    break;
    case VS_INST_2:
    {
        vs_inst_2 vi;
        memcpy(&vi, data, sizeof(vi));

        vd.latitude = vi.latitude;
        vd.longitude = vi.longitude;
        vd.ground_speed = vi.ground_speed;
        vd.satellites_used = vi.sats_used;
        vd.gps_mode = vi.gps_mode;

        ret = true;
    }
    break;
    case VS_POWER:
    {
        vs_power* vp = (vs_power*)data;

        vd.motor_armed = bool(read_bit(vp->switches, 1));
        vd.autothrottle_on = bool(read_bit(vp->switches, 2));
        vd.hil_mode = bool(read_bit(vp->switches, 3));
        vd.has_rc = bool(read_bit(vp->switches, 4));
        vd.inside_boundary = bool(read_bit(vp->switches, 5));
        vd.autolaunch_on = bool(read_bit(vp->switches, 6));

        vd.milliamps = vp->milliamps;
        vd.power_remaining = vp->power_remaining;
        vd.timer = vp->timer;
        vd.volts = vp->volts;
        vd.temperature = vp->temperature;
        vd.ground_track = vp->ground_track;

        ret = true;
    }
    break;
    case VS_AP_STATUS:
    {
        vs_ap_status vas;

        memcpy(&vas, data, sizeof(vas));

        vd.ap_mode = vas.ap_mode;
        vd.ap_alt = vas.ap_alt;
        vd.ap_heading = vas.ap_heading;
        vd.ap_pitch = vas.ap_pitch;
        vd.ap_roll = vas.ap_roll;

        ret = true;
    }
    break;
    case VS_AP_STATUS2:
    {
        vs_ap_status2 vas;
        memcpy(&vas, data, sizeof(vas));

        vd.wp_lat = vas.wp_lat;
        vd.wp_lng = vas.wp_lng;
        vd.wp_number = vas.wp_number;

        ret = true;
    }
    break;
    case VS_RAW_ACC_MAG:
    {
        memcpy(&vd.acc_mag, data, sizeof(vd.acc_mag));
        ret = true;
    }
    break;

    case VC_SATELLITE_REPORT:
    {
        vm_satellite_report vsr;
        memcpy(&vsr, data, sizeof(vsr));
        ret = on_satellite_report(vsr.sat_num, vsr.prn, vsr.azimuth, vsr.elevation, vsr.ss);
    }
    break;
    case VC_HOME_COORDINATES:
    {
        fm_waypoint_pack* vhc = (fm_waypoint_pack*)data;
        ret = on_home_coordinates_report(vhc->lat/1000000.0, vhc->lng/1000000.0, vhc->alt-1000);
    }
    break;
    case VC_AUTOPILOT_STATUS_CHANGE:
    {
        AUTOPILOT_STATUS as;
        memcpy(&as, data, 4);
        ret = on_autopilot_status_change(as);
    }
    break;
    case VC_WAYPOINT_REPORT:
    {
        fm_waypoint_pack* pck = (fm_waypoint_pack*)data;
        ret = on_waypoint_reported(pck->n, pck->lat/1000000.0, pck->lng/1000000.0, pck->alt-1000);
    }
    break;
    case VC_N_WAYPOINTS:
    {
        uint16_t n;
        memcpy(&n, data, 2);
        ret = on_n_waypoints_reported(n);
    }
    break;
    case VC_AHRS_OFFSET_REPORT:
    {
        vm_ahrs_quaternion_report aqr;
        memcpy(&aqr, data, sizeof(aqr));

        float w, x, y, z;
        w = aqr.w/10000.0;
        x = aqr.x/10000.0;
        y = aqr.y/10000.0;
        z = aqr.z/10000.0;

        ret = on_ahrs_offset_reported(w, x, y, z);
    }
    case VC_AHRS_QUATERNION_REPORT:
    {
        int16_t iw, ix, iy, iz;
        float w, x, y, z;

        memcpy(&iw, data, 2);
        memcpy(&ix, data+2, 2);
        memcpy(&iy, data+4, 2);
        memcpy(&iz, data+6, 2);

        w = iw/10000.0;
        x = ix/10000.0;
        y = iy/10000.0;
        z = iz/10000.0;

        ret = on_ahrs_quaternion_reported(w, x, y, z);
    }
    break;
    case VC_MAG_CAL_REPORT:
    {
        ret = on_mag_cal_reported((fm_mag_cal_pack*)(data));
    }
    break;
    case VC_ACC_CAL_REPORT:
    {
        fm_accel_cal_pack* acp = (fm_accel_cal_pack*)data;
        ret = on_acc_cal_reported(acp->x_bias/10000.0, acp->y_bias/10000.0, acp->z_bias/10000.0);
    }
    break;
    case VC_SERVO_CONFIG_REPORT:
    {
        fm_servo_config scr;
        memcpy(&scr, data, sizeof(scr));

        ret = on_servo_configuration_reported(scr.scale/100.0,
                                        scr.ele_min, scr.ele_max,
                                        scr.ail_min, scr.ail_max,
                                        scr.rud_min, scr.rud_max,
                                        scr.thr_min, scr.thr_max);
    }
    break;
    case VC_PITCH_GAINS_REPORT:
    {
        fm_pid_pack pp;
        memcpy(&pp, data, sizeof(pp));
        ret = on_pitch_gains_reported(pp.kp/10000.0, pp.ki/10000.0, pp.kd/10000.0);
    }
    break;
    case VC_ROLL_GAINS_REPORT:
    {
        fm_pid_pack pp;
        memcpy(&pp, data, sizeof(pp));
        ret = on_roll_gains_reported(pp.kp/10000.0, pp.ki/10000.0, pp.kd/10000.0);
    }
    break;
    case VC_HEADING_GAINS_REPORT:
    {
        fm_pid_pack pp;
        memcpy(&pp, data, sizeof(pp));
        ret = on_heading_gains_reported(pp.kp/10000.0, pp.ki/10000.0, pp.kd/10000.0);
    }
    break;
    case VC_VERT_SPEED_GAINS_REPORT:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_vert_speed_gains_reported(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case VC_ALT_GAINS_REPORT:
    {
        fm_pid_pack* pp = (fm_pid_pack*)data;
        ret = on_altitude_gains_reported(pp->kp/10000.0, pp->ki/10000.0, pp->kd/10000.0);
    }
    break;
    case VC_MAX_CLIMB_REPORT:
    {
        fm_max_climb_pack* mc = (fm_max_climb_pack*)data;
        ret = on_max_climb_reported(mc->climb/10.0, mc->decend/10.0);
    }
    break;
    case VC_PATH_GAINS_REPORT:
    {
        fm_pid_pack pp;
        memcpy(&pp, data, sizeof(pp));
        ret = on_path_gains_reported(pp.kp/10000.0, pp.ki/10000.0, pp.kd/10000.0);
    }
    break;
    case VC_AT_GAINS_REPORT:
    {
        fm_pid_pack pp;
        memcpy(&pp, data, sizeof(pp));
        ret = on_at_gains_reported(pp.kp/10000.0, pp.ki/10000.0, pp.kd/10000.0);
    }
    break;
    case VC_AT_SETTINGS_REPORT:
    {
        fm_at_settings* as = (fm_at_settings*)data;
        ret = on_at_settings_reported(as->pt/100.0, as->cs/100.0, as->cp, as->min_thr);
    }
    break;
    case VC_MIXER_CONFIG_REPORT:
    {
        vm_mixer_config_report mcr;
        memcpy(&mcr, data, sizeof(mcr));

        ret = on_mixer_configuration_reported(mcr.config, mcr.mixratio);
    }
    break;
    case VC_MAG_VAR_REPORT:
    {
        vm_mag_var_report mvr;
        memcpy(&mvr, data, sizeof(mvr));
        ret = on_magnetic_variation_reported(mvr.mag_var/100.0);
    }
    break;
    case VC_LOITER_SETTINGS_REPORT:
    {
        loiter_settings_pack lsp;
        memcpy(&lsp, data, sizeof(lsp));
        ret = on_loiter_settings_reported(lsp.loiter_radius, lsp.correction_strength/100.0);
    }
    break;
    case VC_TURN_COORD_REPORT:
    {
        fm_turn_coord_pack* ycp = (fm_turn_coord_pack*)data;
        ret = on_turn_coord_ratios_reported(ycp->yc_ratio/100.0, ycp->bank_pitch_ratio/1000.0);
    }
    break;
    case VC_MAX_ANGLES_REPORT:
    {
        fm_max_angles* ma = (fm_max_angles*)data;
        ret = on_max_angles_reported(ma->pitch_down/100.0, ma->pitch_up/100.0, ma->roll/100.0);
    }
    break;
    case VC_CONTROL_SOURCE_REPORT:
    {
        fm_control_source* csp = (fm_control_source*)data;
        ret = on_control_source_reported(csp->control_source);
    }
    break;
    case VC_BATTERY_MAH_REPORT:
    {
        fm_battery_mah* bah = (fm_battery_mah*)data;
        ret = on_battery_mah_reported(bah->mah);
    }
    break;
    case VC_GEOPOINT_REPORT:
    {
        fm_waypoint_pack* wp = (fm_waypoint_pack*)data;
        ret = on_geofence_point_reported(wp->n, wp->lat/1000000.0, wp->lng/1000000.0);
    }
    break;
    case VC_N_GEOPOINTS:
    {
        vm_n_waypoints* nw = (vm_n_waypoints*)data;
        ret = on_n_geofence_points_reported(nw->n);
    }
    break;
    case VC_ALARM_ACTIONS_REPORT:
    {
        fm_alarm_actions* faa = (fm_alarm_actions*)data;
        ret = on_alarm_actions_reported(faa->low_bat, faa->boundary,
                                        faa->no_gps_lock, faa->lost_control, faa->no_telem);
    }
    break;
    case VC_ALARM_ACTIVATED:
    {
        vm_alarm_activated_pack* ap = (vm_alarm_activated_pack*)data;
        ret = on_alarm_activated(ap->alarm);
    }
    break;
    case VC_MAX_EULER_RATES_REPORT:
    {
        fm_max_euler_rates* mr = (fm_max_euler_rates*)data;
        ret = on_max_euler_rates_reported(mr->pitch/100.0, mr->roll/100.0);
    }
    break;
    case VC_SPEED_GAIN_REPORT:
    {
        int32_t* sg = (int32_t*)data;
        ret = on_speed_gain_reported(*sg / 10000.0);
    }
    break;
    case VC_DEBUG_MSG_FLUSH:
    {
        ret = on_debug_msg_received(vh_msg);
    }
    break;
    }

    if((id >= VC_DEBUG_MSG) && (id < VC_DEBUG_MSG+10)) //if it's a debug message
    {
        int section = id-VC_DEBUG_MSG; //find out which section it is
        memcpy(&vh_msg[section*12], data, 12);
    }

    if(!ret)
    {
        if(Link::read_pack(pack) != -1)
            ret = true;
    }

    //if packet still hasn't been processed, add it to the queue
    if(!ret)
    {
        PackQueue* ptr = &in_queue;
        while(ptr->pNext != 0)
            ptr = ptr->pNext;

        ptr->pNext = (PackQueue*)malloc(sizeof(PackQueue));
        ptr->pNext->pb.id = id;
        memcpy(&ptr->pNext->pb.data, data, 12);
        ptr->pNext->pNext = 0;
    }

    return id;
}

int Station::in_queue_size()
{
    PackQueue* ptr = &in_queue;
    uint8_t ret = 0;
    while(ptr->pNext)
    {
        ptr = ptr->pNext;
        ret++;
    }
    return ret;
}

bool Station::on_in_queue(uint16_t id)
{
    PackQueue* ptr = &in_queue;
    while(ptr->pNext)
    {
        if(ptr->pNext->pb.id == id)
            return true;
        ptr = ptr->pNext;
    }
    return false;
}

bool Station::remove_from_in_queue(uint16_t id)
{
    PackQueue* ptr = &in_queue;
    while(ptr->pNext)
    {
        if(ptr->pNext->pb.id == id)
        {
            PackQueue* tmpptr = ptr->pNext;
            ptr->pNext = ptr->pNext->pNext;
            free(tmpptr);
            return true;
        }
        ptr = ptr->pNext;
    }
    return false;
}

void Station::clear_in_queue()
{
    PackQueue* ptr = &in_queue;
    while(ptr->pNext)
    {
        PackQueue* tmpptr = ptr->pNext;
        ptr->pNext = ptr->pNext->pNext;
        free(tmpptr);
    }
}

fl_pack Station::retrieve_from_in_queue(uint16_t id)
{
    PackQueue* ptr = &in_queue;
    fl_pack ret;

    while(ptr->pNext)
    {
        if(ptr->pNext->pb.id == id)
        {
            ret = ptr->pNext->pb;
            PackQueue* tmpptr = ptr->pNext;
            ptr->pNext = ptr->pNext->pNext;
            free(tmpptr);
            return ret;
        }
        ptr = ptr->pNext;
    }
    return ret;
}

};
