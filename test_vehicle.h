#ifndef TV_H
#define TV_H


#include "firelink.h"


using namespace FireLink;


class myvehicle : public FireLink::Vehicle
{
    public:
        myvehicle(VehicleData* vd);
        bool on_keep_alive_recv(uint32_t s);
        bool on_arm_motor_recv(bool arm);
        bool on_add_waypoint(uint16_t n, float lat, float lng, int32_t alt);
        bool on_add_geofence_point(uint16_t n, float lat, float lng);
        bool on_get_geofence_point(uint16_t n);
        bool on_get_n_geofence_points();
        bool on_remove_geopoint(uint16_t n);
        bool on_clear_geopoints();
        bool on_set_alarm_actions(ALARM_ACTION lb,
                                        ALARM_ACTION ba,
                                        ALARM_ACTION gpsa,
                                        ALARM_ACTION lca,
                                        ALARM_ACTION no_telem);
        bool on_get_alarm_actions();


        bool on_set_max_euler_rates(uint16_t pitch, uint16_t roll);
        bool on_get_max_euler_rates();


        bool on_set_speed_gain(float sg);
        bool on_get_speed_gain();

        bool on_get_servo_configuration();

        bool on_set_turn_coord_ratios(float r, float p);

        bool on_set_vert_speed_gains(float kp, float ki, float kd);
        bool on_get_vert_speed_gains();

        bool on_set_accel_cal(float x, float y, float z);
        bool on_get_accel_cal();

        bool on_get_mag_cal();
        bool on_set_mag_cal(fm_mag_cal_pack* f);

        bool on_set_selected_heading_and_alt(int16_t hdg, int32_t alt);
        bool on_set_selected_pitch_and_roll(int16_t pitch, int16_t roll);

        float vs_kp, vs_ki, vs_kd;

        float acc_x, acc_y, acc_z;

        FireLink::fm_mag_cal_pack mcp;

        uint32_t keep_alive_seconds;
        float home_lat, home_lng, home_alt;
        float ahrs_w, ahrs_x, ahrs_y, ahrs_z;

        int last_wp_n;
        float last_wp_lat, last_wp_lng;
        int last_wp_alt;

        int last_gp_n;
        float last_gp_lat, last_gp_lng;

        bool geopoints_cleared;
        uint16_t last_remove_geopoint;

        ALARM_ACTION low_bat_action;
        ALARM_ACTION boundary_action;
        ALARM_ACTION no_gps_action;
        ALARM_ACTION lost_control_action;
        ALARM_ACTION no_telem_action;

        uint16_t max_pitch_rate, max_roll_rate;
        float speed_gain;

        int sel_pitch, sel_roll, sel_heading, sel_alt;
};



#endif // TV_H
