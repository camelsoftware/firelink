#ifndef CAMLIB_TEST_H_INCLUDED
#define CAMLIB_TEST_H_INCLUDED

#include <iostream>
#include <vector>
#include <string>

#include "SmrtPtr.h"

namespace Camlib
{

class TestHarness
{
public:
    TestHarness(std::string t)
    {
        title = t;
    }

    bool run()
    {
        std::cout << "BEGIN TEST: " << title << std::endl << std::endl;
        std::vector< Camlib::SmrtPtr<TestModule> >::iterator it = tm_.begin();
        while(it != tm_.end())
        {
            Camlib::SmrtPtr<TestModule> ptr = *it;
            if(!ptr->run())
            {
                std::cout << "Aborting test" << std::endl;
                return false;
            }
            it++;
        }

        std::cout << std::endl << "END TEST" << std::endl;
        return true;
    }

    void add_module(bool (*test)(std::string& msg), std::string t)
    {
        Camlib::SmrtPtr<TestModule> ptm = new TestModule(test, t);
        tm_.push_back(ptm);
    }

private:
    class TestModule
    {
    public:
        TestModule(bool (*tes)(std::string& msg), std::string t)
        {
            title = t;
            title.resize(60, '.');
            title.resize(75, '.');
            test = tes;
        }

        bool run()
        {
            std::cout << title;
            std::string msg;
            if(test(msg))
                std::cout << " pass" << std::endl;
            else
            {
                std::cout << " fail\n\tMessage: " << msg << std::endl;
                return false;
            }

            return true;
        }


    private:
        std::string title;
        bool (*test)(std::string& sec);
    };

    std::string title;
    std::vector< Camlib::SmrtPtr<TestModule> > tm_;
};

};


#endif // TEST_H_INCLUDED
