#ifndef SMRTPTR_H_INCLUDED
#define SMRTPTR_H_INCLUDED

#include <memory>

namespace Camlib
{

template <typename T> class SmrtPtr
{
public:
    SmrtPtr() : pData(0), ref(0)
    {
        ref = new RefCounter();
        ref->reference();
    }

    SmrtPtr(T* pValue) : pData(pValue)
    {
        ref = new RefCounter();
        ref->reference();
    }

    SmrtPtr(const SmrtPtr<T>& sp) : pData(sp.pData), ref(sp.ref)
    {
        ref->reference();
    }

    ~SmrtPtr()
    {
        if(ref->unreference() == 0)
        {
            delete pData;
            delete ref;
        }
    }

    T& operator* ()
    {
        return *pData;
    }

    T* operator-> ()
    {
        return pData;
    }

    SmrtPtr<T>& operator = (const SmrtPtr<T>& sp)
    {
        if (this != &sp)
        {
            if(ref->unreference() == 0)
            {
                delete pData;
                delete ref;
            }

            pData = sp.pData;
            ref = sp.ref;
            ref->reference();
        }
        return *this;
    }

    bool operator == (const SmrtPtr<T>& sp)
    {
        return pData == sp.pData;
    }

    bool operator != (const SmrtPtr<T>& sp)
    {
        return pData != sp.pData;
    }

    operator bool()
    {
        if(pData)
            return true;
        return false;
    }

    private:

    class RefCounter
    {
    public:
        void reference()
        {
            count++;
        }

        int unreference()
        {
            return --count;
        }

    private:
        int count; // Reference count
    };

    T* pData;
    RefCounter* ref;
};

};

#endif // SMRTPTR_H_INCLUDED
