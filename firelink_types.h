/**
 * \file firelink_types.h
 * \brief FireLink packet structs and enums
 */

/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef FIRELINK_ENUMS_H_INCLUDED
#define FIRELINK_ENUMS_H_INCLUDED


namespace FireLink
{

//possible sources for altitude
/**
 * \brief Possible altimeter sources
 */
enum ALTIMETER_SOURCE : uint8_t
{
    ALT_GPS,    /*!< \brief GPS with a 3d lock */
    ALT_BARO,   /*!< \brief Barometric altimeter */
    ALT_BOTH    /*!< \brief Fused baro alt and GPS sensors */
};


/**
 * \brief Possible autopilot statuses
 */
enum AUTOPILOT_STATUS : uint8_t
{
    AP_STATUS_INITIALISE, /*!< Autopilot is still initialising (waiting for GPS lock, AHRS stablisation, etc) */
    AP_STATUS_NORMAL, /*!< Autopilot is in operating normally */
    AP_STATUS_ERROR /*!< There is an error or abnormal situation */
};


/**
 * \brief Autopilot modes
 */
enum AUTOPILOT_MODE : uint8_t
{
    AP_MODE_MANUAL, /*!< Manual */
    AP_MODE_TRAINER, /*!< Manual with some assistance */
    AP_MODE_PITCH_AND_ROLL, /*!< Control inputs move the autopilot pitch & roll bugs */
    AP_MODE_HEADING_AND_ALTITUDE, /*!< Control inputs move the heading & altitude bugs */
    AP_MODE_LOITER, /*!< The vehicle will stay in its current location */
    AP_MODE_RETURN_TO_BASE, /*!< The vehicle will return to its base coordinates */
    AP_MODE_WAYPOINTS /*!< The vehicle will move along its waypoint list */
};

/**
 * \brief Possible GPS modes. Taken from NMEA sentances.
 */
enum GPS_MODE : uint8_t
{
    GPS_MODE_NONE, /*!< GPS mode is unknown */
    GPS_MODE_NO_LOCK, /*!< No GPS lock */
    GPS_MODE_2D_LOCK, /*!< 2D GPS lock */
    GPS_MODE_3D_LOCK /*!< 3D GPS lock, with altitude */
};

/**
 * \brief Channel mixing configurations.
 */
enum MIXER_CONFIGURATION : uint8_t
{
    MIXER_CONFIG_NONE,
    MIXER_CONFIG_V_TAIL,
    MIXER_CONFIG_FLYING_WING
};

/**
 * \brief Control input sources.
 */
enum CONTROL_SOURCE : uint8_t
{
    CONTROL_SOURCE_FIRELINK,
    CONTROL_SOURCE_PWM
};


/**
 * \brief Alarm types. These are usually abnormal events or situations
 * that require operator notification and possibly an automatic response.
 * Low bat means the vehicle is getting concerned about battery charge.
 * Boundary alarm means the vehicle has intersected the geofence.
 * GPS no lock is self-explanatory.
 * No settings means the vehicle couldn't load settings.
 * Lost control alarm means the control source (RC Tx/Rx) has dropped out (not triggered in autonomous modes).
 */
enum ALARM_TYPE : uint8_t
{
    ALARM_TYPE_LOW_BAT,
    ALARM_TYPE_BOUNDARY,
    ALARM_TYPE_GPS_NOLOCK,
    ALARM_TYPE_NO_SETTINGS,
    ALARM_TYPE_LOST_CONTROL,
    ALARM_TYPE_LOST_DATALINK
};


/**
 * \brief Boundary collision actions.
 */
enum ALARM_ACTION : uint8_t
{
    ALARM_ACTION_NONE,
    ALARM_ACTION_LOCKUP,
    ALARM_ACTION_NO_PWR_GLIDE,
    ALARM_ACTION_MANUAL_CTRL,
    ALARM_ACTION_FIXED_BANK_LOITER,
    ALARM_ACTION_RTB,
    ALARM_ACTION_LOITER
};

enum FLIGHT_COMMAND : uint8_t
{
    FLIGHT_COMMAND_NEXT_WAYPOINT,
    FLIGHT_COMMAND_BACK_WAYPOINT,
    FLIGHT_COMMAND_RESTART
};

/**
 * \brief Generic PID gain packet.
 */
struct fm_pid_pack
{
    uint32_t kp;
    uint32_t ki;
    uint32_t kd;
};

struct fm_max_angles
{
    int16_t pitch_down;
    int16_t pitch_up;
    int16_t roll;
};

struct fm_at_settings
{
    int16_t pt; //pitch to throttle linkage in tenths
    int16_t cs; //cruise speed in tenths of km/h
    int8_t cp; //cruise power setting as a percentage.
    int8_t min_thr; //minimum allowed throttle setting
};

struct fm_control_source
{
    CONTROL_SOURCE control_source;
};

struct fm_alarm_actions
{
    ALARM_ACTION boundary;
    ALARM_ACTION low_bat;
    ALARM_ACTION no_gps_lock;
    ALARM_ACTION lost_control;
    ALARM_ACTION no_telem;
};

struct fm_battery_mah
{
    uint32_t mah;
};

struct fm_waypoint_pack
{
    int32_t lat;
    int32_t lng;
    uint16_t alt;
    uint16_t n;
};

struct fm_max_euler_rates
{
    int32_t pitch; //in hundredths of degrees per second
    int32_t roll;
};


struct fm_servo_config
{
    int16_t scale; /*!< RC signal scale factor in hundredths.*/
    int8_t ele_min;
    int8_t ele_max;
    int8_t ail_min;
    int8_t ail_max;
    int8_t rud_min;
    int8_t rud_max;
    int8_t thr_min;
    int8_t thr_max;
};


struct fm_turn_coord_pack
{
    uint16_t yc_ratio; /*!< Yaw coupling ratio in hundredths of a percent. 10000 = 100.00%, 5050 = 50.50% */
    uint16_t bank_pitch_ratio; /*!<Pitch to bank ratio */
};

struct fm_max_climb_pack
{
    uint32_t climb; /*!< Climb rate in tenths of feet per minute. */
    uint32_t decend; /*!< Decent rate in tenths of feet per minute. */
};

struct fm_accel_cal_pack
{
    int32_t x_bias; /*< Thousandths in units of gravity. */
    int32_t y_bias;
    int32_t z_bias;
};

struct fm_mag_cal_pack
{
    int16_t x_gain; /*< In units of whatevaaaaa. */
    int16_t y_gain;
    int16_t z_gain;

    int16_t x_bias;
    int16_t y_bias;
    int16_t z_bias;
};

struct fm_select_pnr_pack
{
    int16_t pitch;
    int16_t roll;
};

struct fm_select_hna_pack
{
    int16_t heading;
    int32_t alt;
};

struct vs_inst_1
{
    int16_t heading;
    int16_t pitch;
    int16_t roll;
    uint16_t baroalt;
    int16_t vert_speed;
    uint16_t air_speed;
};

struct vs_inst_2
{
    int32_t latitude;
    int32_t longitude;
    uint16_t ground_speed;
    uint8_t sats_used;
    GPS_MODE gps_mode;
};

struct vs_power
{
    //each bit of the switches variable represents the state of a switch
    //bit 1 = motor armed
    //bit 2 = autothrottle on
    //bit 3 = HIL mode
    //bits 4-8 are spare, currently
    uint8_t switches;

    uint8_t power_remaining;
    uint16_t temperature;
    uint16_t volts;
    uint16_t timer;
    uint16_t milliamps;
    int16_t ground_track; //an odd place to put this, but it's the only place it will fit
};

struct vs_ap_status
{
    AUTOPILOT_MODE ap_mode;
    int16_t ap_pitch;
    int16_t ap_roll;
    int16_t ap_heading;
    int32_t ap_alt;
};

struct vs_ap_status2
{
    int32_t wp_lat;
    int32_t wp_lng;
    uint16_t wp_number;
};

struct vs_raw_acc_mag
{
    int16_t mag_x;
    int16_t mag_y;
    int16_t mag_z;
    int16_t acc_x;
    int16_t acc_y;
    int16_t acc_z;
};

/**
 * \brief Vehicle streaming packet types
 */
enum VS_Packs : uint16_t
{
    VS_INST_1, /*!< Instruments type 1 */
    VS_INST_2, /*!< Instruments type 2 */
    VS_POWER, /*!< Power information */
    VS_AP_STATUS, /*!< Autopilot status */
    VS_AP_STATUS2,
    VS_RAW_ACC_MAG,
    VS_NULL
};


/**
 * \brief VC_SATELLITE_REPORT - satellite report.
 * This packet provides information about a particular satellite. It is sent in response to SC_GET_SATELLITE_REPORT.
 */
struct vm_satellite_report
{
    int8_t sat_num; /*!< Number of satellite being reported */
    uint16_t azimuth; /*!< Satellite azimuth */
    uint16_t elevation; /*!< Satellite elevation */
    uint16_t ss; /*!< Satellite signal to noise ratio. Zero if the satellite isn't in use. */
    int32_t prn; /*!< Pseudorandom noise - satellite identification */
};


/**
 * \brief VC_AUTOPILOT_STATUS_CHANGE - autopilot status change.
 * This packet is sent whenever the autopilot changes status. It will be sent in reponse to SV_GET_AUTOPILOT_STATUS or whenever the status changes.
 */
struct vm_autopilot_status_change
{
    AUTOPILOT_STATUS as; /*!< Autopilot status */
};


/**
 * \brief VC_N_WAYPOINTS - number of waypoints report
 * This packet is sent in response to SC_GET_N_WAYPOINTS
 */
struct vm_n_waypoints
{
    uint16_t n; /*!< Number of waypoints uploaded to the vehicle */
};


/**
 * \brief VC_MAG_VAR_REPORT - magnetic variation report
 * This packet reports the current magnetic variation to the ground station.
 * It is sent in response to SC_GET_MAG_VAR.
 */
struct vm_mag_var_report
{
    int32_t mag_var; /*!< Magnetic variation in hundredths of degrees. Positive numbers are east, negative numbers are west. */
};


/**
 * \brief VC_AHRS_OFFSET_REPORT, VC_AHRS_QUATERNION_REPORT - ahrs quaternion report
 * This packet contains a quaternion expressed in ten-thousands.
 * VC_AHRS_OFFSET_REPORT is sent in response to SC_GET_AHRS_INSTALLATION_OFFSET.
 * VC_AHRS_QUATERNION_REPORT is sent in response to SC_GET_AHRS_QUATERNION
 */
struct vm_ahrs_quaternion_report
{
    int16_t w, x, y, z;
};


/**
 * \brief VC_MIXER_CONFIG_REPORT - mixer configuration report
 * This packet reports mixer configuration. It is sent in response to SC_GET_MIXER_CONFIG.
 */
struct vm_mixer_config_report
{
    MIXER_CONFIGURATION config; /*!< Mixer configuration (standard, V-tail, flying wing). */
    uint16_t mixratio; /*!< Mix ratio. 0 - 100 where is more elevator. */
};


/**
 * \brief VC_LOITER_SETTINGS_REPORT, SC_SET_LOITER_SETTINGS - the settings used in loiter mode.
 */
struct loiter_settings_pack
{
    uint32_t loiter_radius; /*!< Loiter radius in meters. */
    uint32_t correction_strength; /*!< Correction strength in hundredths. */
};


struct vm_alarm_activated_pack
{
    ALARM_TYPE alarm;
};


/**
 * \brief Vehicle message packet types.
 */
enum VM_Packs : uint16_t
{
    VC_FLUSH = 128,
    VC_SATELLITE_REPORT,
    VC_HOME_COORDINATES,
    VC_AUTOPILOT_STATUS_CHANGE,
    VC_WAYPOINT_REPORT,
    VC_N_WAYPOINTS,
    VC_MAG_VAR_REPORT,
    VC_AHRS_OFFSET_REPORT,
    VC_AHRS_QUATERNION_REPORT,
    VC_MAG_CAL_REPORT,
    VC_ACC_CAL_REPORT,
    VC_SERVO_CONFIG_REPORT,
    VC_PITCH_GAINS_REPORT,
    VC_ROLL_GAINS_REPORT,
    VC_HEADING_GAINS_REPORT,
    VC_VERT_SPEED_GAINS_REPORT,
    VC_ALT_GAINS_REPORT,
    VC_MAX_CLIMB_REPORT,
    VC_PATH_GAINS_REPORT,
    VC_AT_GAINS_REPORT,
    VC_AT_SETTINGS_REPORT,
    VC_MIXER_CONFIG_REPORT,
    VC_LOITER_SETTINGS_REPORT,
    VC_TURN_COORD_REPORT,
    VC_MAX_ANGLES_REPORT,
    VC_CONTROL_SOURCE_REPORT,
    VC_BATTERY_MAH_REPORT,
    VC_GEOPOINT_REPORT,
    VC_N_GEOPOINTS,
    VC_ALARM_ACTIONS_REPORT, //reports the actions that the aircraft will take when an alarm is activated
    VC_ALARM_ACTIVATED, //gets sent when an alarm is activated
    VC_MAX_EULER_RATES_REPORT,
    VC_SPEED_GAIN_REPORT,
    VC_DEBUG_MSG,
    VC_DEBUG_MSG_FLUSH = VC_DEBUG_MSG+10
};

/**
 * \brief Station streaming packets
 */
enum SS_Packs : uint16_t
{
    SS_CONTROL = 1
};

/**
 * \brief SC_KEEP_ALIVE - the keep alive packet forces the vehicle to keep sending data for a specified number of seconds.
 * If no packets are received and the defib time runs out, the vehicle will stop transmitting.
 * SC_KEEP_ALIVE forces the datalink to keep going even when the vehicle isn't receiving good packets.
 * The idea is that the vehicle should only transmit when there is a station within range, and the station should only
 * need to transmit a packet once every few seconds to get the vehicle transmitting. This is a power saving thing.
 */
struct sm_keep_alive
{
    uint32_t seconds; /*!< Number of seconds to transmit for */
};

/**
 * \brief SC_ARM_MOTOR - this packet arms and disarms the motor(s).
 * Disarming the motor whilst doing maintenance can be useful and might save someones finger.
 * Note: it is the responsibility of the vehicle designer to ensure that a disarmed motor CAN NOT OPERATE under any circumstances.
 */
struct sm_arm_motor
{
    bool arm; /*!< If true, the motor should be armed and able to operate normally. If false, the motor be disarmed and OFF at all times. */
};


/**
 * \brief SC_SET_AP_MODE - this packet changes the autopilot mode.
 */
struct sm_set_ap_mode
{
    AUTOPILOT_MODE mode; /*!< The selected autopilot mode */
};

/**
 * \brief SC_GET_SATELLITE_REPORT - this packet GETs a report on a particular satellite.
 * The vehicle should send a VC_SATELLITE_REPORT packet in response
 */
struct sm_get_satellite_report
{
    uint8_t sat_n; /*!< Satellite number - maximum legitimate value is VehicleData::satellites_used */
};

/**
 * \brief SC_SET_ALTIMETER_SOURCE - switches source of altitude.
 */
struct sm_set_altimeter_source
{
    ALTIMETER_SOURCE alt_source; /*!< Altitude source */
};

/**
 * \brief SC_REMOVE_WAYPOINT - removes a waypoint from the vehicles waypoint list
 */
struct sm_remove_waypoint
{
    uint16_t waypoint; /*!< Waypoint to remove */
};

/**
 * \brief SC_GET_WAYPOINT - GETs a report on a waypoint.
 * The vehicle will reply with a VC_WAYPOINT_REPORT packet.
 */
struct sm_get_waypoint
{
    uint16_t waypoint; /*!< Waypoint number */
};

/**
 * \brief SC_SET_MAG_VAR - sets the magnetic variation
 */
struct sm_set_mag_var
{
    int32_t mag_var; /*!< Magnetic variation in hundredths of a degree. Negative numbers are west, positive numbers are east. */
};

/**
 * \brief SC_SET_AHRS_INSTALLATION_OFFSET - sets the installation offset of the AHRS.
 * This packet should only be used during calibration / setup.
 */
struct sm_set_ahrs_installation_offset
{
    int16_t w, x, y, z; /*!< Floating point quaternion values in ten-thousandths. (assign values to a float and divide by 10000) */
};

/**
 * \brief SC_SET_MIXER_CONFIG - sets the mixer configuration.
 */
struct sm_set_mixer_config
{
    MIXER_CONFIGURATION config; /*!< Mixer configuration (standard, V-tail, flying wing). */
    uint8_t mixratio; /*!< Mix ratio. 0 - 100 where is more elevator. */
};

struct sm_hil_mode
{
    uint8_t hm; //0 = switch off, 1 = switch on
    int8_t pwd[8]; //secret key to switch hil mode on
};

struct sm_autothrottle
{
    uint8_t on;
};


/**
 * \brief SM_Packs - station message packet types
 */
enum SM_Packs : uint16_t
{
	SC_FLUSH = 128,
	SC_KEEP_ALIVE,
    SC_ARM_MOTOR,
    SC_SET_AP_MODE,
    SC_GET_AUTOPILOT_STATUS,
    SC_GET_SATELLITE_REPORT,
    SC_SET_ALTIMETER_SOURCE, //check enum ALTIMETER_SOURCE
    SC_GET_HOME_COORDINATES,
    SC_SET_HOME_COORDINATES,
    SC_CLEAR_WAYPOINTS, //remove all waypoints from vehicle
    SC_REMOVE_WAYPOINT,
    SC_GET_N_WAYPOINTS,
    SC_GET_MAG_VAR,
    SC_SET_MAG_VAR,
    SC_GET_AHRS_INSTALLATION_OFFSET,
    SC_SET_AHRS_INSTALLATION_OFFSET,
    SC_GET_AHRS_QUATERNION, //GETs a quaternion from the ahrs
    SC_CALIBRATE_MAGNETOMETERS, //
    SC_GET_MAGNETOMETER_CAL, //
    SC_SET_MAGNETOMETER_CAL, //
    SC_GET_ACCELEROMETER_CAL,
    SC_SET_ACCELEROMETER_CAL,
    SC_GET_SERVO_CONFIG,
    SC_SET_SERVO_CONFIG,
    SC_GET_PITCH_GAINS,
    SC_SET_PITCH_GAINS,
    SC_GET_ROLL_GAINS,
    SC_SET_ROLL_GAINS,
    SC_GET_HEADING_GAINS,
    SC_SET_HEADING_GAINS,
    SC_GET_VERT_SPEED_GAINS,
    SC_SET_VERT_SPEED_GAINS,
    SC_GET_ALT_GAINS,
    SC_SET_ALT_GAINS,
    SC_GET_MAX_CLIMB,
    SC_SET_MAX_CLIMB,
    SC_GET_PATH_GAINS,
    SC_SET_PATH_GAINS,
    SC_GET_AT_GAINS,
    SC_SET_AT_GAINS,
    SC_GET_AT_SETTINGS,
    SC_SET_AT_SETTINGS,
    SC_GET_MIXER_CONFIG,
    SC_SET_MIXER_CONFIG,
    SC_GET_LOITER_SETTINGS,
    SC_SET_LOITER_SETTINGS,
    SC_GET_TURN_COORD_RATIOS,
    SC_SET_TURN_COORD_RATIOS,
    SC_GET_MAX_ANGLES,
    SC_SET_MAX_ANGLES,
    SC_HIL_MODE,
    SC_AUTOTHROTTLE,
    SC_AUTOLAUNCH,
    SC_SET_CONTROL_SOURCE,
    SC_GET_CONTROL_SOURCE,
    SC_SET_BATTERY_MAH,
    SC_GET_BATTERY_MAH,
    SC_ADD_WAYPOINT,
    SC_ADD_WAYPOINT2,
    SC_GET_WAYPOINT,
    SC_GET_WAYPOINT2,
    SC_ADD_GEOPOINT,
    SC_ADD_GEOPOINT2,
    SC_GET_GEOPOINT,
    SC_GET_GEOPOINT2,
    SC_GET_N_GEOPOINTS,
    SC_REMOVE_GEOPOINT,
    SC_CLEAR_GEOPOINTS,
    SC_SET_ALARM_ACTIONS,
    SC_GET_ALARM_ACTIONS,
    SC_SET_MAX_EULER_RATES,
    SC_GET_MAX_EULER_RATES,
    SC_SET_SPEED_GAIN,
    SC_GET_SPEED_GAIN,
    SC_SET_PNR_MAX_INTS, //TODO
    SC_GET_PNR_MAX_INTS, //TODO
    SC_SET_STREAM_MASK,
    SC_FLIGHT_COMMAND,
    SC_SET_PNR,
    SC_SET_HNA
};


}

#endif // FIRELINK_ENUMS_H_INCLUDED
